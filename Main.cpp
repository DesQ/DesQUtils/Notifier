/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <iostream>
#include <unistd.h>

#include <DFXdg.hpp>
#include <DFUtils.hpp>
#include <DFNotifyWatcher.hpp>

#include <DFCoreApplication.hpp>
#include <DFApplication.hpp>

#include <desq/Utils.hpp>
#include <desq/desq-config.h>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>

#include "ClientGlobal.hpp"
#include "ServerGlobal.hpp"

#include "MainClient.hpp"
#include "MainServer.hpp"

#include "UI.hpp"
#include "Misc.hpp"

QStringList allowedHints = {
    "category:string",
    "desktop-entry:string",
    "image-path:string",
    "resident:boolean",
    "transient:boolean",
    "urgency:byte",
    "percent:double",
    "action:string",
};

QStringList capabilities = {
    /* "action-icons", */
    "actions",
    "body",
    "body-hyperlinks",
    "body-images",
    "body-markup",
    "icon-static",
    /* "icon-multi", */
    "persistence",
    /* "sound", */
    "inline-reply",
};

int main( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Notifier.log" ).toLocal8Bit().data(), "a" );
    std::atexit(
        []() {
            if ( DFL::log ) {
                fclose( DFL::log );
            }
        }
    );

    qInstallMessageHandler( DFL::Logger );

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Notifier started at" << QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    QStringList arguments;
    for ( int i = 0 ; i < argc; i++ ) {
        arguments << argv[ i ];
    }

    QCommandLineParser parser;
    createCLI( &parser );

    /* Process the CLI args */
    parser.parse( arguments );

    /** Run */
    if ( parser.isSet( "server" ) ) {
        /** The user has passed other options. We will ignore them. */
        if ( parser.optionNames().count() > 1 ) {
            QString unused;
            for ( QString opt: parser.optionNames() ) {
                if ( opt != "server" ) {
                    unused += QString( " %1" ).arg( opt );
                }
            }
            qWarning() << "Ignoring the following unused options:";
            qWarning() << "  " << unused;
        }

        qInfo() << "Starting server...";
        return runServerCode( argc, argv );
    }

    if ( parser.isSet( "help" ) ) {
        showHelp();

        return 0;
    }

    if ( parser.isSet( "version" ) ) {
        showVersion();

        return 0;
    }

    if ( parser.isSet( "capabilities" ) ) {
        QStringList serverCaps = getServerCapabilities();
        for ( QString cap: serverCaps ) {
            std::cout << cap.toStdString() << std::endl;
        }

        return 0;
    }

    if ( parser.isSet( "server-info" ) ) {
        QStringList serverInfo = getServerInformation();

        if ( serverInfo.count() == 4 ) {
            std::cout << "Name:         " << serverInfo.at( 0 ).toStdString() << std::endl;
            std::cout << "Vendor:       " << serverInfo.at( 1 ).toStdString() << std::endl;
            std::cout << "Version:      " << serverInfo.at( 2 ).toStdString() << std::endl;
            std::cout << "Spec Version: " << serverInfo.at( 3 ).toStdString() << std::endl;
        }

        return 0;
    }

    /** If no notifier service is running, attempt to start desq-notifier as the server */
    if ( isServiceRunning() == false ) {
        qDebug() << argv[ 0 ];

        if ( QProcess::startDetached( argv[ 0 ], QStringList( { "--server" } ), QDir::homePath() ) ) {
            qCritical() << "Failed to start the server";
            return 1;
        }

        /** Artificial delay to ensure that the server is running and ready. */
        sleep( 1 );
    }

    /** By now we should have a server running!! */
    if ( isServiceRunning() ) {
        QStringList serverInfo = getServerInformation();

        /** Not DesQ Notifier */
        if ( serverInfo.value( 1 ) != "DesQ" ) {
            qWarning() << "Another notification service is running.";
            qWarning() << "It may not support all the features supported by DesQ Notifier";
        }

        /** Do our work */
        return runClientCode( argc, argv, &parser );
    }

    else {
        qCritical() << "This notification will not be shown.";
        qCritical() << "Unable to detect a running notification server.";
    }

    return 1;
}
