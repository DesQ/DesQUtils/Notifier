/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Misc.hpp"
#include "Factory.hpp"
#include "Bubble.hpp"
#include "BubbleImpl.hpp"
#include "BubbleUtils.hpp"

#include "ServerGlobal.hpp"
#include "ThemeManager.hpp"

#include <desq/Utils.hpp>

#include <DFHjsonParser.hpp>
#include <desqui/LayoutManager.hpp>

#include "Buttons.hpp"
#include "Factory.hpp"
#include "Labels.hpp"
#include "LineEdit.hpp"
#include "ProgressBar.hpp"

/**
 * Bubble Base
 */

BubbleBase::BubbleBase( uint id, uint urgency ) : QWidget() {
    mId      = id;
    mUrgency = urgency;
    timer    = new DesQ::Timer( this );

    connect(
        timer, &DesQ::Timer::timeout, [ = ] () {
            emit expired( mId );
        }
    );
}


BubbleBase::~BubbleBase() {
    timer->stop();
    delete timer;
}


uint BubbleBase::urgency() {
    return mUrgency;
}


void BubbleBase::startTimer( uint interval ) {
    if ( timer->isActive() ) {
        timer->stop();
    }

    timer->start( interval );
}


void BubbleBase::pause() {
    timer->pause();
}


void BubbleBase::resume() {
    /** We want to show the notification for at least 2 seconds */
    if ( timer->remainingTime() < 2000 ) {
        startTimer( 2000 );
    }

    else {
        timer->resume();
    }
}


void BubbleBase::stop() {
    if ( timer->isActive() ) {
        timer->stop();
    }
}


void BubbleBase::enterEvent( QMouseEnterEvent *eEvent ) {
    timer->pause();
    QWidget::enterEvent( eEvent );
}


void BubbleBase::leaveEvent( QEvent *lEvent ) {
    timer->resume();
    QWidget::leaveEvent( lEvent );
}


void BubbleBase::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        mPressed = true;
    }

    mEvent->accept();
}


void BubbleBase::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( mPressed && ( mEvent->button() == Qt::LeftButton ) ) {
        mPressed = false;

        /** We're done with the timer */
        timer->stop();

        /** Inform the parent that this widget was clicked */
        emit clicked( mId );
    }

    mEvent->accept();
}


/**
 * Bubble UI
 */

DesQ::Notifier::Bubble::Bubble( DFL::NotifyWatcher::Notification n, QWidget *parent ) : QWidget( parent ) {
    setObjectName( QString( "%1/%2" ).arg( n.appName ).arg( n.id ) );

    setFixedWidth( bubbleWidth );

    /** Whatever sizeHint returns is the best possible size */
    setSizePolicy( QSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed ) );

    QIcon icon = getIconForNotification( n );

    wFactory->setNotification( &n );

    QVariantMap headerLytMap = themeMgr->headerLayout( n );
    QVariantMap footerLytMap = themeMgr->footerLayout( n );

    headerLytMap[ "Properties" ] = updateAppIcon( headerLytMap[ "Properties" ].toMap(), icon );
    footerLytMap[ "Properties" ] = updateAppIcon( footerLytMap[ "Properties" ].toMap(), icon );

    if ( n.hints.contains( "percent" ) ) {
        headerLytMap[ "Properties" ] = updateProgressValue( headerLytMap[ "Properties" ].toMap(), n.hints );
        footerLytMap[ "Properties" ] = updateProgressValue( footerLytMap[ "Properties" ].toMap(), n.hints );
    }

    baseLyt = new QVBoxLayout();
    baseLyt->setSpacing( 0 );
    baseLyt->setContentsMargins( QMargins() );

    QBoxLayout *headerLyt = nullptr;
    QBoxLayout *footerLyt = nullptr;

    if ( headerLytMap.isEmpty() == false ) {
        headerLyt = lytMgr->generateLayout( headerLytMap );
        headerLyt->setObjectName( "Header" );
    }

    if ( footerLytMap.isEmpty() == false ) {
        footerLyt = lytMgr->generateLayout( footerLytMap );
        footerLyt->setObjectName( "Footer" );
    }

    // QBoxLayout *bodyLyt = prepareBody( n );
    BubbleBase *body = prepareBody( n );
    body->setParent( this );

    if ( headerLyt != nullptr ) {
        baseLyt->addLayout( headerLyt );
    }

    // baseLyt->addLayout( bodyLyt );
    baseLyt->addWidget( body );

    if ( footerLyt != nullptr ) {
        baseLyt->addLayout( footerLyt );
    }

    setLayout( baseLyt );
    // setFixedHeight( baseLyt->sizeHint().height() );

    /** Useful when we have a close button in header or a footer */
    QToolButton *tb = headerLyt->findChild<QToolButton *>( "CloseButton" );

    if ( tb == nullptr ) {
        tb = footerLyt->findChild<QToolButton *>( "CloseButton" );
    }

    if ( tb != nullptr ) {
        connect(
            tb, &QToolButton::clicked, [ = ] () {
                for ( uint id: idBubbleMap.keys() ) {
                    /** We're dismissing (so why = 2) all bubbles */
                    close( id, 2 );
                }
            }
        );
    }

    /** Shortcut to hide the counter, if it exists */
    updateHiddenCount();

    mSizeHint = baseLyt->sizeHint();
}


void DesQ::Notifier::Bubble::expand( DFL::NotifyWatcher::Notification n ) {
    /** Prepare the body */
    BubbleBase *body = prepareBody( n );

    /** Pause this timer now. Resume it when we show this notification */
    body->pause();

    /** The supports hiding notifications */
    if ( themeMgr->maximumVisibleCount() ) {
        /**
         * We have at least one notification showing. If the visible notification is
         * a low priority one, hide it.
         */
        BubbleBase *hidden = hideLastNotification( baseLyt, 0 );

        if ( hidden ) {
            mPendingLow.push_front( hidden );

            /** Update the sizehint */
            mSizeHint -= QSize( 0, hidden->sizeHint().height() );
        }

        /**
         * Get the number of visible notifications
         */
        uint visible = getNotificationCount( baseLyt, 3 );

        /**
         * If we have at least one visible, and the current notification is low priority, hide it.
         * We have nothing more to do here!
         */
        if ( visible && ( n.hints[ "urgency" ] == 0 ) ) {
            mPendingLow.push_front( body );

            /** Update the hidden count */
            updateHiddenCount();

            return;
        }

        /**
         * If the number of critical notifications equal/exceed maximumVisibleCount,
         * Hide this notification if it's a normal one.
         */
        uint criticalCount = getNotificationCount( baseLyt, 2 );

        if ( ( criticalCount >= themeMgr->maximumVisibleCount() ) && ( body->urgency() == 1 ) ) {
            mPendingNormal.push_front( body );

            /** Update the hidden count */
            updateHiddenCount();

            return;
        }

        /**
         * So, we do not have enough critical notifications to fill up the space.
         * Which means, we should check if we need to hide a normal notification.
         */
        uint normalCount = getNotificationCount( baseLyt, 1 );

        if ( criticalCount + normalCount == themeMgr->maximumVisibleCount() ) {
            hidden = hideLastNotification( baseLyt, 1 );

            if ( hidden ) {
                mPendingNormal.push_front( hidden );

                /** Update the sizehint */
                mSizeHint -= QSize( 0, hidden->sizeHint().height() );
            }
        }

        /**
         * At this stage, we have three possibilities:
         * (1) Nothing is shown, and we have one notification of any type in limbo.
         * (2) One or more notifications visible, with space for a normal or critical notification in limbo.
         * (3) More than maximumVisibleCount notifications, and a critical notification in limbo.
         * In any case, we simply proceed to show the notification that is in limbo.
         */
    }

    /** Place the notification at the correct position */
    moveToVisible( body, indexForUrgency( body->urgency() ) );

    /** Update the hidden count */
    updateHiddenCount();
}


void DesQ::Notifier::Bubble::replace( DFL::NotifyWatcher::Notification n ) {
    /** Security check. Should not be needed */
    if ( idBubbleMap.contains( n.id ) == false ) {
        return;
    }

    /** Delete the previous body layout */
    BubbleBase *oldBody = idBubbleMap.take( n.id );

    /** Get the newly prepared body */
    BubbleBase *body = prepareBody( n );

    for ( int i = 0; i < baseLyt->count(); i++ ) {
        /** We have the correct layout */
        if ( (BubbleBase *)baseLyt->itemAt( i )->widget() == oldBody ) {
            /** Stop the old timer */
            oldBody->stop();

            /** Delete the item layout */
            deleteLayoutItem( baseLyt->takeAt( i ) );

            /** Insert the newly generated layout */
            baseLyt->insertWidget( i, body );

            /** We're done replacing. No need to iterate further */
            return;
        }
    }
}


void DesQ::Notifier::Bubble::close( uint id, uint why, QString action ) {
    /** Security check. Should not be needed */
    if ( idBubbleMap.contains( id ) == false ) {
        qWarning() << "No such ID" << id;
        qWarning() << "Active IDs" << idBubbleMap.keys();
        return;
    }

    /** Delete the previous body layout */
    BubbleBase *oldBody = idBubbleMap.take( id );

    for ( int i = 0; i < baseLyt->count(); i++ ) {
        if ( baseLyt->itemAt( i )->widget() == oldBody ) {
            /** Stop the old timer */
            oldBody->stop();

            /** Correct the size */
            mSizeHint -= QSize( 0, oldBody->sizeHint().height() );

            /** Delete the item */
            deleteLayoutItem( baseLyt->takeAt( i ) );

            /** If there was an action, emit it */
            if ( action.length() ) {
                emit actionInvoked( id, action );
            }

            /** Emit the notification closed signal */
            emit closed( id, why );

            /** We're done closing. No need to iterate further */
            break;
        }
    }

    /** Show pending normal notification only if visibleCount < maximumVisibleCount */
    if ( getNotificationCount( baseLyt, 3 ) < themeMgr->maximumVisibleCount() ) {
        /** Attempt to move notifications from mPendingNormal to visible */
        if ( mPendingNormal.count() ) {
            /** Get the widget */
            BubbleBase *base = mPendingNormal.takeFirst();

            /** Place the widget */
            moveToVisible( base, indexForUrgency( 1 ) );

            /** Force the UI to resize */
            emit resizeRequired();

            /** Update the hidden notification count */
            updateHiddenCount();

            /** Done. We have atleast one notification: return! */
            return;
        }
    }

    /** Show pending low notification only if there are no other visible items */
    if ( getNotificationCount( baseLyt, 3 ) == 0 ) {
        /** Attempt to move notifications from mPendingLow to visible */
        if ( mPendingLow.count() ) {
            /** Get the widget */
            BubbleBase *base = mPendingLow.takeFirst();

            /** Place the widget */
            moveToVisible( base, indexForUrgency( 0 ) );

            /** Force the UI to resize */
            emit resizeRequired();

            /** Update the hidden notification count */
            updateHiddenCount();

            /** Done. We have atleast one notification: return! */
            return;
        }
    }

    /** No pending: close the bubble if there are no more bodies!! */
    if ( idBubbleMap.count() ) {
        return;
    }

    /** No more bodies */
    QWidget::close();

    /** Tell the overlords! */
    emit burstBubble( objectName() );
}


int DesQ::Notifier::Bubble::activeCount() {
    return idBubbleMap.count();
}


int DesQ::Notifier::Bubble::hasId( uint id ) {
    return idBubbleMap.keys().contains( id );
}


QSize DesQ::Notifier::Bubble::sizeHint() const {
    return mSizeHint;
}


BubbleBase * DesQ::Notifier::Bubble::prepareBody( DFL::NotifyWatcher::Notification n ) {
    QIcon icon = getIconForNotification( n );

    wFactory->setNotification( &n );

    /** Obtain the layout map */
    QVariantMap bodyLytMap = themeMgr->bodyLayout( n );

    /** Update a few properties: App icon, and percentage */
    bodyLytMap[ "Properties" ] = updateAppIcon( bodyLytMap[ "Properties" ].toMap(), icon );
    bodyLytMap[ "Properties" ] = updateProgressValue( bodyLytMap[ "Properties" ].toMap(), n.hints );

    /** Generate the layout */
    QBoxLayout *bodyLyt = lytMgr->generateLayout( bodyLytMap );

    BubbleBase *body = new BubbleBase( n.id, n.hints[ "urgency" ].toUInt() );

    /** Set the object name and property */
    body->setObjectName( QString( "Body/%1" ).arg( n.id ) );
    body->setProperty( "ID",      n.id );
    body->setProperty( "Urgency", n.hints[ "urgency" ] );

    /** Set the layout */
    body->setLayout( bodyLyt );

    /** Add action for close button: dismissed */
    QToolButton *tb = body->findChild<QToolButton *>( "CloseButton" );

    if ( tb != nullptr ) {
        connect(
            tb, &QToolButton::clicked, [ = ] () {
                /** Dismiss (so, why = 2) this notification, and close it */
                close( n.id, 2 );
            }
        );
    }

    /** Action was invoked */
    QList<QPushButton *> actionBtns = body->findChildren<QPushButton *>( "ActionButton" );
    for ( QPushButton *ab: actionBtns ) {
        connect(
            ab, &QPushButton::clicked, [ = ] () {
                /** Invoke the action and close the bubble. */
                close( n.id, 2, ab->property( "Action" ).toString() );
            }
        );
    }

    /** Insert it into the map */
    idBubbleMap[ n.id ] = body;

    if ( n.timeout > 0 ) {
        body->startTimer( n.timeout );

        connect(
            body, &BubbleBase::expired, [ = ] ( uint id ) {
                /** Close first!! Otherwise, it will not be resized properly */
                emit close( id, 1 );
            }
        );
    }

    connect(
        body, &BubbleBase::clicked, [ = ] ( uint id ) {
            /** Close first!! Otherwise, it will not be resized properly */
            emit close( id, 2, "default" );
        }
    );

    return body;
}


int DesQ::Notifier::Bubble::indexForUrgency( uint urgency ) {
    bool bodyFound = false;

    for ( int i = 0; i < baseLyt->count(); i++ ) {
        BubbleBase *item = qobject_cast<BubbleBase *>( baseLyt->itemAt( i )->widget() );

        /** We encountered a nullptr, */
        if ( item == nullptr ) {
            /** and we've gone beyond all the bodies */
            if ( bodyFound ) {
                return i;
            }

            /** and there are no bodies, because bodies were hidden */
            else if ( i == baseLyt->count() - 1 ) {
                return i;
            }

            /** but it's before we've reached a body */
            else {
                continue;
            }
        }

        /** Okay, item is not nullptr. So we found a body */
        bodyFound = true;

        /** If the level matches the required level, then we insert it here */
        if ( item->urgency() == urgency ) {
            return i;
        }

        /** We have a lower urgency. This means that required urgency wasn't there. */
        else if ( item->urgency() < urgency ) {
            return i;
        }

        /** Current urgency is greater than the required urgency. Go to next. */
        else {
            continue;
        }
    }

    return baseLyt->count();
}


void DesQ::Notifier::Bubble::moveToVisible( BubbleBase *base, int i ) {
    /** Insert it */
    baseLyt->insertWidget( i, base );

    /** Update the size hint */
    mSizeHint += QSize( 0, base->sizeHint().height() );

    /** Resume the timer */
    base->resume();
}


void DesQ::Notifier::Bubble::updateHiddenCount() {
    NotifyCount *nc = findChild<NotifyCount *>( "NotifyCount" );

    if ( nc ) {
        int hidden = mPendingLow.length() + mPendingNormal.length();

        if ( hidden ) {
            nc->show();
            nc->setText( QString( "and %1 other" ).arg( hidden ) );
        }

        else {
            nc->hide();
            nc->setText( "" );
        }
    }
}


void DesQ::Notifier::Bubble::paintEvent( QPaintEvent *pEvent ) {
    // QColor baseClr = themeMgr->baseColor();
    QColor baseClr( 0, 0, 0, 77 );
    QColor borderClr = themeMgr->borderColor();
    qreal  radius    = themeMgr->borderRadius();

    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );

    if ( baseClr.isValid() ) {
        painter.setPen( QPen( borderClr ) );
        painter.setBrush( baseClr );

        painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), radius, radius );
    }

    QWidget::paintEvent( pEvent );

    if ( borderClr.isValid() ) {
        painter.setPen( QPen( borderClr ) );
        painter.setBrush( Qt::NoBrush );

        painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), radius, radius );
    }

    painter.end();
}
