/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "ClientGlobal.hpp"
#include "ServerGlobal.hpp"

class BubbleBase;

/**
 * A simple hack to delete the widget encoded in the QLayoutItem.
 * Extract the widget, and set the parent to null. Then add to a dummy layout.
 * Finally, delete the layout.
 */
void deleteLayoutItem( QLayoutItem *item );

/**
 * Get Notificaiton Icon
 * A notification may not specifically set the icon.
 * In such cases, try to get it from appName, or desktop name, etc.
 */
QIcon getIconForNotification( DFL::NotifyWatcher::Notification n );

/**
 * An icon may not have been set by the notification directly.
 * So, once we get an icon, insert the icon into the properties.
 */
QVariantMap updateAppIcon( QVariantMap props, QIcon icon );

/**
 * Update the properties layout map with the correct value of progress.
 * Check if "percent" hint is set, and the we have a "ProgressBar" key in @props map.
 */
QVariantMap updateProgressValue( QVariantMap props, QVariantMap hints );

/**
 * A helper function to extract the last notification of urgency @type from @baseLyt.
 * If we did not find any notification of @type level, return a nullptr, which means
 * nothing was extracted.
 */
BubbleBase * hideLastNotification( QBoxLayout *baseLyt, uint type );

/**
 * Count the number of visible notifications of @urgency level in @baseLyt
 * Though we will be returning values >= 0, we will use int, so that we don't have to
 * cast other things elsewhere.
 * Special case: urgency == 3 will count all notification types
 */
uint getNotificationCount( QBoxLayout *baseLyt, uint urgency );
