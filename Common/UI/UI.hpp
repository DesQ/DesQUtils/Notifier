/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "ClientGlobal.hpp"
#include "ServerGlobal.hpp"

#include <DFNotifyWatcher.hpp>

#include <desqui/LayoutManager.hpp>

#include <wayqt/LayerShell.hpp>

#include "Factory.hpp"
#include "ThemeManager.hpp"

namespace DesQ {
    namespace Notifier {
        class Bubble;
        class UI;
    }
}

class DesQ::Notifier::UI : public QWidget {
    Q_OBJECT;

    public:
        UI();

        Q_SLOT uint addNotification( DFL::NotifyWatcher::Notification );
        Q_SLOT void removeNotification( uint id );

        Q_SLOT void show();

        void handleMessages( QString message, int fd );

    private:

        /**
         * DFL::NotifyWatcher watches for incoming notfications
         * and will inform the UI about them. It will also inform
         * if some notifications were requested to be closed.
         */
        DFL::NotifyWatcher *watcher;

        /**
         * Notification count
         */
        static uint notifyCount;

        /**
         * A map of app names and notification bubbles.
         * This will help us to group notifications.
         */
        QHash<QString, DesQ::Notifier::Bubble *> appBubbleMap;

        /**
         * Map the bubble's objectName to appName.
         * This will help us to burst the bubbles.
         */
        QHash<QString, QString> bubbleIdAppMap;

        /**
         * The layout to which all notification bubbles will be added
         */
        QVBoxLayout *baseLyt;

        /**
         * Active notifications, so that we can replace some if necessary
         */
        QList<uint> mActiveList;

        /**
         * LayerSurface object so that it can be resized
         * Also, allied variables
         */
        WQt::LayerSurface *cls = nullptr;
        WQt::LayerSurface::SurfaceAnchors clsAnchors;
        QMargins clsMargins;

        /**
         * Resize the notification UI.
         * This entails resizing the LayerSurface as well
         */
        void resizeUI();

    Q_SIGNALS:
        void NotificationReplied( uint, QString );
        void NotificationClosed( uint, uint );
        void ActionInvoked( uint, QString );

    protected:
        void paintEvent( QPaintEvent * ) override;
};
