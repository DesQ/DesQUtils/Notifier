/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "ClientGlobal.hpp"
#include "ServerGlobal.hpp"

#include <desq/Timer.hpp>
#include <DFNotifyWatcher.hpp>

#if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
    #define QMouseEnterEvent    QEvent
#else
    #define QMouseEnterEvent    QEnterEvent
#endif

namespace DesQ {
    namespace Notifier {
        class Bubble;
    }
}

class BubbleBase;

/** Classic Bubble */
class DesQ::Notifier::Bubble : public QWidget {
    Q_OBJECT;

    public:
        Bubble( DFL::NotifyWatcher::Notification, QWidget * );

        /** Add a notification to this bubble */
        void expand( DFL::NotifyWatcher::Notification );

        /** Replace the existing notification with this one */
        void replace( DFL::NotifyWatcher::Notification );

        /** Close a notification. Emit closed(...) signal. Optionally emit actionInvoked */
        void close( uint id, uint why, QString action = QString() );

        /** Do we have any active notifications in this one? */
        int activeCount();

        /** Is this ID being shown currently? */
        int hasId( uint );

        /** Override the size hint */
        QSize sizeHint() const override;

    private:
        QHash<uint, BubbleBase *> idBubbleMap;

        /** Normal and Low priority pending notifications */
        QList<BubbleBase *> mPendingNormal;
        QList<BubbleBase *> mPendingLow;

        /** Prepare the BubbleBase widget for the notification */
        BubbleBase *prepareBody( DFL::NotifyWatcher::Notification );

        /** Find the correct position to insert a notification with level @urgency */
        int indexForUrgency( uint urgency );

        /** Insert the notification @base at @idx */
        void moveToVisible( BubbleBase *base, int idx );

        /** Search if we have a NotifyCount widget, and update the count */
        void updateHiddenCount();

        QVBoxLayout *baseLyt;

        bool mPressed = false;

        QSize mSizeHint = QSize( bubbleWidth, 10 );

    protected:
        void paintEvent( QPaintEvent * ) override;

    Q_SIGNALS:
        void closed( uint id, uint why );
        void burstBubble( QString );
        void actionInvoked( uint, QString );

        void resizeRequired();
};
