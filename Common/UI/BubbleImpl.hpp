/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "ClientGlobal.hpp"
#include "ServerGlobal.hpp"

#include <desq/Timer.hpp>

#if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
    #define QMouseEnterEvent    QEvent
#else
    #define QMouseEnterEvent    QEnterEvent
#endif

/** Classic Bubble */
class BubbleBase : public QWidget {
    Q_OBJECT;

    public:
        BubbleBase( uint id, uint urgency );

        /** To delete the timer pointer */
        ~BubbleBase();

        /** Urgency of the notification shown here */
        uint urgency();

        /** Start the timer for this notification */
        void startTimer( uint );

        /** Pause the timer */
        void pause();

        /** Resume the timer */
        void resume();

        /** Stop the timer */
        void stop();

    protected:
        /** Mouse enter and leave events to pause/resume timer */
        void enterEvent( QMouseEnterEvent *eEvent );
        void leaveEvent( QEvent *lEvent );

        /** To detect dismissed */
        void mousePressEvent( QMouseEvent *mEvent );
        void mouseReleaseEvent( QMouseEvent *mEvent );

    private:
        DesQ::Timer *timer;
        uint mId      = 0;
        uint mUrgency = 0;

        bool mPressed = false;

    Q_SIGNALS:
        void expired( uint id );
        void clicked( uint id );
};
