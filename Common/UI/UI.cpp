/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "UI.hpp"
#include "Misc.hpp"
#include "Bubble.hpp"
#include "ServerGlobal.hpp"

#include <wayqt/LayerShell.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/WayQtUtils.hpp>

#include <DFHjsonParser.hpp>

/** The first 500 IDs are reserved for use with DesQ */
uint DesQ::Notifier::UI::notifyCount = 500;

/**
 * Notification UI
 */

DesQ::Notifier::UI::UI() : QWidget() {
    watcher = new DFL::NotifyWatcher( "addNotification", this );

    watcher->setAvailableHints( allowedHints );
    watcher->setCapabilities( capabilities );
    watcher->setServerInformation(
        {
            "desq-notifier",
            "DesQ",
            PROJECT_VERSION,
            "1.2"
        }
    );

    connect( watcher, &DFL::NotifyWatcher::CloseNotificationRequest, this, &DesQ::Notifier::UI::removeNotification );

    setFixedWidth( bubbleWidth );
    setMinimumHeight( iconLabelSize );

    setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint | Qt::BypassWindowManagerHint );
    setAttribute( Qt::WA_TranslucentBackground );

    baseLyt = new QVBoxLayout();
    baseLyt->setContentsMargins( QMargins() );
    baseLyt->setSpacing( 2 );
    baseLyt->addStretch();

    setLayout( baseLyt );
}


uint DesQ::Notifier::UI::addNotification( DFL::NotifyWatcher::Notification n ) {
    /** Increment the count only if it's not a replacement */
    if ( n.id == 0 ) {
        /** Increment by 1 */
        notifyCount++;

        /** We need a unique ID. Ensure that the number chosen is not active */
        while ( mActiveList.contains( notifyCount ) ) {
            /** Reset the ID counter: First 500 are reserved */
            if ( notifyCount > INT_MAX ) {
                notifyCount = 501;
            }

            notifyCount++;
        }
    }

    /**
     * Notification ID given: ensure it exists; exit if it does not!
     * Of course, we should honor DesQ Apps.
     */
    else {
        if ( ( mActiveList.contains( n.id ) == false ) && ( n.id > 500 ) ) {
            qWarning() << "No notifications exist with id" << n.id;
            qWarning() << "Do not specify an ID while requesting to show notifications.";

            return 0;
        }
    }

    /** Give a proper ID if it's a fresh notification */
    n.id = ( n.id ? n.id : notifyCount );

    /**
     * Ensure we have a notification timeout:
     *   - If urgency = 2: timeout = 0
     *   - else: timeout > 0
     * timeout is in ms.
     */

    // If the urgency is 2, then blindly set the timeout to 0.
    if ( n.hints[ "urgency" ].toInt() == 2 ) {
        n.timeout = 0;
    }

    // If the timeout is -1, set it to default time out.
    n.timeout = ( n.timeout == -1 ? defaultTimeOut : n.timeout );

    // Overrides: If resident is true, do not expire
    if ( n.hints[ "resident" ].toBool() == true ) {
        n.timeout = 0;
    }

    /** If n.id exists in mActiveList, then we replace that notification */
    if ( mActiveList.contains( n.id ) ) {
        appBubbleMap[ n.appName ]->replace( n );

        resizeUI();
        return n.id;
    }

    /** New notification */
    mActiveList << n.id;

    /** Prepare the actions: They come as a linear list of even length; pair them. */
    QStringList actions;

    if ( n.actions.length() ) {
        if ( n.actions.length() % 2 == 1 ) {
            n.actions.removeLast();
        }

        for ( int i = 0; i < n.actions.length(); i += 2 ) {
            actions << QString( "%1:%2" ).arg( n.actions.value( i + 1 ), n.actions.value( i ) );
        }

        n.actions = actions;
    }

    /**
     * If the current theme supports grouping,
     * and we have an active notification for this app,
     * then, group this notification with the other one.
     */
    if ( themeMgr->supportsGrouping() && appBubbleMap.contains( n.appName ) ) {
        /** Move widget to top */
        baseLyt->removeWidget( appBubbleMap[ n.appName ] );
        baseLyt->insertWidget( 0, appBubbleMap[ n.appName ] );

        appBubbleMap[ n.appName ]->expand( n );

        resizeUI();
        return n.id;
    }

    /** Treat this as a notification from a fresh app: Create a new bubble */
    DesQ::Notifier::Bubble *bubble = new DesQ::Notifier::Bubble( n, this );

    /** The notification with ID @id was closed with reason @why */
    connect(
        bubble, &DesQ::Notifier::Bubble::closed, [ = ]( uint id, uint why ) {
            mActiveList.removeAll( id );
            emit NotificationClosed( id, why );

            resizeUI();
        }
    );

    /** Forward the actionInvoked signal to the Adaptor */
    connect( bubble, &DesQ::Notifier::Bubble::actionInvoked,  this, &DesQ::Notifier::UI::ActionInvoked );

    /** The bubble showed a previously hidden notification. So resize the UI. */
    connect( bubble, &DesQ::Notifier::Bubble::resizeRequired, this, &DesQ::Notifier::UI::resizeUI );

    /** All notifications from this bubble are closed. Remove the bubble from the UI. */
    connect(
        bubble, &DesQ::Notifier::Bubble::burstBubble, [ = ]( QString appName ) {
            QString appBubbleId       = bubbleIdAppMap.take( appName );
            DesQ::Notifier::Bubble *b = appBubbleMap.take( appBubbleId );
            baseLyt->removeWidget( b );
            delete b;

            resizeUI();
        }
    );

    /** All new notifications go to the top of the layout. */
    baseLyt->insertWidget( 0, bubble );
    appBubbleMap[ n.appName ] = bubble;
    bubbleIdAppMap[ QString( "%1/%2" ).arg( n.appName ).arg( n.id ) ] = n.appName;

    if ( cls ) {
        resizeUI();
    }

    else {
        show();
    }

    return n.id;
}


void DesQ::Notifier::UI::removeNotification( uint id ) {
    if ( mActiveList.contains( id ) == false ) {
        return;
    }

    for ( DesQ::Notifier::Bubble *bbl: appBubbleMap.values() ) {
        if ( bbl->hasId( id ) ) {
            bbl->close( id, 3 );

            break;
        }
    }

    if ( appBubbleMap.count() == 0 ) {
        delete cls;
        cls = nullptr;

        close();
    }
}


void DesQ::Notifier::UI::show() {
    QWidget::show();

    /** This is exclusively for wayfire - requires wayfire dbusqt plugin */
    if ( WQt::Utils::isWayland() ) {
        WQt::LayerShell::LayerType lyr = WQt::LayerShell::Overlay;
        cls = wlRegistry->layerShell()->getLayerSurface( windowHandle(), nullptr, lyr, "de-widget" );

        switch ( (int)notifySett->value( "Position" ) ) {
            case 1: { // Bottom Right
                clsAnchors = WQt::LayerSurface::Bottom | WQt::LayerSurface::Right;

                /** 10 px gap on bottom and right */
                clsMargins = QMargins( 0, 0, 10, 10 );

                break;
            }

            case 2: { // Bottom Left
                clsAnchors = WQt::LayerSurface::Bottom | WQt::LayerSurface::Left;

                /** 10 px gap on bottom and left */
                clsMargins = QMargins( 10, 0, 0, 10 );

                break;
            }

            case 3: { // Top Left
                clsAnchors = WQt::LayerSurface::Top | WQt::LayerSurface::Left;

                /** 10 px gap on top and left */
                clsMargins = QMargins( 0, 10, 0, 10 );

                break;
            }

            case 4: { // Top Center
                clsAnchors = WQt::LayerSurface::Top;

                /** 10 px gap on top */
                clsMargins = QMargins( 0, 10, 0, 0 );

                break;
            }

            case 5: { // Right Center
                clsAnchors = WQt::LayerSurface::Right;

                /** 10 px gap on right */
                clsMargins = QMargins( 0, 0, 10, 0 );

                break;
            }

            case 6: { // Bottom Center
                clsAnchors = WQt::LayerSurface::Bottom;

                /** 10 px gap on bottom */
                clsMargins = QMargins( 0, 0, 0, 10 );

                break;
            }

            case 7: { //Left Center
                clsAnchors = WQt::LayerSurface::Left;

                /** 10 px gap on left */
                clsMargins = QMargins( 10, 0, 0, 0 );

                break;
            }

            default: { // Same as Top Right
                clsAnchors = WQt::LayerSurface::Top | WQt::LayerSurface::Right;

                /** 10 px gap on top and right */
                clsMargins = QMargins( 0, 10, 10, 0 );

                break;
            }
        }

        /** Set anchors and margins */
        cls->setAnchors( clsAnchors );
        cls->setMargins( clsMargins );

        /** Size of our surface */
        cls->setSurfaceSize( size() );

        /** Feel free to move this to make place for others */
        cls->setExclusiveZone( 0 );

        /** We may need keyboard interaction (in future). Nothing needed now */
        cls->setKeyboardInteractivity( WQt::LayerSurface::NoFocus );

        /** Commit to our choices */
        cls->apply();

        /** Resize the UI */
        resizeUI();
    }
}


void DesQ::Notifier::UI::handleMessages( QString, int ) {
    // Nothing to do now.
}


void DesQ::Notifier::UI::resizeUI() {
    /** No active notifications */
    if ( appBubbleMap.count() == 0 ) {
        /** Delete the layer surface */
        delete cls;
        cls = nullptr;

        /** Close the widget */
        close();
    }

    else {
        /**
         * There will be N - 1 spacings of 2px (N = number of bubbles)
         * We will be adding 2px per bubble => 2px extra. Reduce that
         * at the beginning itself;
         */
        int height = -2;

        for ( DesQ::Notifier::Bubble *b: appBubbleMap.values() ) {
            height += b->sizeHint().height() + 2;
        }

        setFixedHeight( height );

        /** We'll assume that the notification bubble is still visible */
        cls->setAnchors( clsAnchors );
        cls->setMargins( clsMargins );
        cls->setSurfaceSize( size() );

        cls->apply();
    }
}


void DesQ::Notifier::UI::paintEvent( QPaintEvent *pEvent ) {
    QWidget::paintEvent( pEvent );
    // QPainter painter( this );
    //
    // painter.setRenderHints( QPainter::Antialiasing );
    // painter.setPen( QPen( Qt::yellow, 1.0 ) );
    //
    // painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 5.0, 5.0 );
    // painter.end();
}
