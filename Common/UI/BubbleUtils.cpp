/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Bubble.hpp"
#include "BubbleImpl.hpp"
#include "BubbleUtils.hpp"

#include "Buttons.hpp"
#include "Factory.hpp"
#include "Labels.hpp"
#include "LineEdit.hpp"
#include "ProgressBar.hpp"

#include <DFXdg.hpp>

void deleteLayoutItem( QLayoutItem *item ) {
    QVBoxLayout *w = new QVBoxLayout();

    item->widget()->setParent( nullptr );
    w->addWidget( item->widget() );

    delete w;
}


QIcon getIconForNotification( DFL::NotifyWatcher::Notification n ) {
    QIcon icon = QIcon::fromTheme( n.appIcon, QIcon( n.appIcon ) );

    /** QIcon::isNull() doesn't work with QIcon( path ) */
    if ( icon.pixmap( iconImageSize ).width() <= 0 ) {
        /** Check if we can extract an icon from the app name */
        icon = QIcon::fromTheme( n.appName );
    }

    /** QIcon::isNull() doesn't work with QIcon( path ) */
    if ( icon.pixmap( iconImageSize ).width() <= 0 ) {
        /** Check if we have a desktop entry */
        if ( n.hints.contains( "desktop-entry" ) ) {
            DFL::XDG::DesktopFile desktop( n.hints[ "desktop-entry" ].toString() + ".desktop" );
            icon = QIcon::fromTheme( desktop.icon(), QIcon( desktop.icon() ) );
        }

        /** May be appName itself is the desktop name */
        else {
            DFL::XDG::DesktopFile desktop( n.appName + ".desktop" );
            icon = QIcon::fromTheme( desktop.icon(), QIcon( desktop.icon() ) );
        }
    }

    /** QIcon::isNull() doesn't work with QIcon( path ) */
    if ( icon.pixmap( iconImageSize ).width() <= 0 ) {
        icon = QIcon::fromTheme( "dialog-information" );
    }

    return icon;
}


QVariantMap updateAppIcon( QVariantMap props, QIcon icon ) {
    if ( props.contains( "AppIcon" ) ) {
        QVariantMap appIcon = props[ "AppIcon" ].toMap();

        /** We can try to get the "name". But who cares. */
        appIcon[ "QIcon" ] = icon;

        props[ "AppIcon" ] = appIcon;
    }

    return props;
}


QVariantMap updateProgressValue( QVariantMap props, QVariantMap hints ) {
    if ( hints.contains( "percent" ) ) {
        if ( props.contains( "ProgressBar" ) ) {
            QVariantMap progress = props[ "ProgressBar" ].toMap();
            progress[ "Value" ]    = hints[ "percent" ].toDouble();
            props[ "ProgressBar" ] = progress;
        }
    }

    return props;
}


BubbleBase * hideLastNotification( QBoxLayout *baseLyt, uint type ) {
    /**
     * Recurse in the reverse order. Last item is the lowest notification.
     * The first notification we get of the type will be the one to be hidden.
     */
    for ( int i = baseLyt->count() - 1; i >= 0; i-- ) {
        BubbleBase *item = qobject_cast<BubbleBase *>( baseLyt->itemAt( i )->widget() );

        if ( item == nullptr ) {
            continue;
        }

        /** This is the last notification. If it's normal one, hide it */
        if ( item->urgency() == type ) {
            BubbleBase *base = qobject_cast<BubbleBase *>( baseLyt->takeAt( i )->widget() );

            base->pause();
            base->setParent( nullptr );

            return base;
        }
    }

    return nullptr;
}


uint getNotificationCount( QBoxLayout *baseLyt, uint urgency ) {
    uint count = 0;

    for ( int i = 0; i < baseLyt->count(); i++ ) {
        BubbleBase *item = qobject_cast<BubbleBase *>( baseLyt->itemAt( i )->widget() );

        /** We encountered a nullptr, */
        if ( item == nullptr ) {
            continue;
        }

        /** Special case */
        if ( urgency == 3 ) {
            count++;
        }

        /** If the level matches the required level, then we insert it here */
        else if ( item->urgency() == urgency ) {
            count++;
        }
    }

    return count;
}
