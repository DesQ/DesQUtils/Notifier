/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ThemeManager.hpp"
#include "ThemeUtils.hpp"

#include <DFInotify.hpp>
#include <DFHjsonParser.hpp>

// TODO: Priorities need to be set for theme paths.

QMap<QString, QString> DesQ::Notifier::ThemeManager::mAvailableThemes;

static inline QMap<QString, QString> readThemeNames( QString path ) {
    QMap<QString, QString> themes;

    QDir        themeDir( path );
    QStringList themeNames = themeDir.entryList( QDir::Dirs | QDir::NoDotAndDotDot );

    for ( QString themeName: themeNames ) {
        QString themePath = path + "/" + themeName;

        if ( QFile::exists( themePath + "/index.theme" ) ) {
            themes[ themeName ] = themePath;
        }
    }

    return themes;
}


static inline QColor getColor( QVariant colorVar ) {
    switch ( colorVar.userType() ) {
        case QMetaType::QVariantList: {
            [[fallthrough]];
        }

        case QMetaType::QStringList: {
            QVariantList rgba = colorVar.toList();

            int red   = -1;
            int green = -1;
            int blue  = -1;
            int alpha = -1;

            /** RGB only */
            if ( rgba.length() == 3 ) {
                QString r = rgba.at( 0 ).toString();
                QString g = rgba.at( 1 ).toString();
                QString b = rgba.at( 2 ).toString();

                red   = ceil( r.contains( "." ) ? r.toDouble() * 255 : r.toDouble() );
                green = ceil( g.contains( "." ) ? g.toDouble() * 255 : g.toDouble() );
                blue  = ceil( b.contains( "." ) ? b.toDouble() * 255 : b.toDouble() );
            }

            else if ( rgba.length() == 4 ) {
                QString r = rgba.at( 0 ).toString();
                QString g = rgba.at( 1 ).toString();
                QString b = rgba.at( 2 ).toString();
                QString a = rgba.at( 3 ).toString();

                red   = ceil( r.contains( "." ) ? r.toDouble() * 255 : r.toDouble() );
                green = ceil( g.contains( "." ) ? g.toDouble() * 255 : g.toDouble() );
                blue  = ceil( b.contains( "." ) ? b.toDouble() * 255 : b.toDouble() );
                alpha = ceil( a.contains( "." ) ? a.toDouble() * 255 : a.toDouble() );
            }

            else {
                return QColor();
            }

            return QColor( red, green, blue, alpha );
        }

        /** #RRGGBBAA or 0xRRGGBBAA string or named color */
        case QMetaType::QString: {
            QString rgba = colorVar.toString();

            if ( rgba.startsWith( "0x" ) ) {
                rgba = rgba.replace( "0x", "#" );
            }

            return QColor( rgba );
        }

        default: {
            return QColor();
        }
    }

    return QColor();
}


DesQ::Notifier::ThemeManager::ThemeManager() {
    fsWatcher = new DFL::Inotify();

    connect(
        fsWatcher, &DFL::Inotify::nodeCreated, [ = ] ( QString nodePath ) {
            /** Add if does not exist */
            QString name = QFileInfo( nodePath ).baseName();

            if ( mAvailableThemes.contains( name ) == false ) {
                if ( QFile::exists( nodePath + "/index.theme" ) ) {
                    mAvailableThemes[ name ] = nodePath;
                }
            }
        }
    );

    connect(
        fsWatcher, &DFL::Inotify::nodeDeleted, [ = ] ( QString nodePath ) {
            /** Remove if it exists */
            QString name = QFileInfo( nodePath ).baseName();

            if ( mAvailableThemes.contains( name ) ) {
                mAvailableThemes.remove( name );
            }
        }
    );

    connect(
        fsWatcher, &DFL::Inotify::nodeChanged, [ = ] ( QString nodePath ) {
            /** Update theme if the current theme changed */
            QString name = QFileInfo( nodePath ).baseName();

            if ( mThemeName == name ) {
                setThemeName( name );
            }
        }
    );

    connect(
        fsWatcher, &DFL::Inotify::nodeRenamed, [ = ] ( QString oldPath, QString newPath ) {
            /** Update theme */
            QString oldName = QFileInfo( oldPath ).baseName();
            QString newName = QFileInfo( newPath ).baseName();

            if ( mAvailableThemes.contains( oldName ) ) {
                mAvailableThemes.remove( oldName );
                mAvailableThemes[ newName ] = newPath;
            }

            /** Update to use the newly renamed thene */
            if ( mThemeName == oldName ) {
                setThemeName( newName );
            }
        }
    );

    for ( QString themePath: themePaths ) {
        fsWatcher->addWatch( themePath, DFL::Inotify::PathOnly );
    }
}


DesQ::Notifier::ThemeManager::~ThemeManager() {
    fsWatcher->stopWatch();

    delete fsWatcher;
}


bool DesQ::Notifier::ThemeManager::addThemePath( QString path ) {
    QFileInfo info( path );

    if ( info.exists() && info.isDir() ) {
        themePaths << info.absoluteFilePath() + "/";
        mAvailableThemes.insert( readThemeNames( path ) );

        return true;
    }

    return false;
}


QStringList DesQ::Notifier::ThemeManager::availableThemes() {
    /** When we have already read the available themes */
    if ( mAvailableThemes.count() ) {
        return mAvailableThemes.keys();
    }

    /** First time */
    for ( QString themePath: themePaths ) {
        mAvailableThemes.insert( readThemeNames( themePath ) );
    }

    return mAvailableThemes.keys();
}


QString DesQ::Notifier::ThemeManager::themeName() {
    return mThemeName;
}


void DesQ::Notifier::ThemeManager::setThemeName( QString themeName ) {
    /** First ascertain that this theme exists */
    if ( availableThemes().contains( themeName ) == false ) {
        qWarning() << themeName << "is not a valid theme name. Using 'classic' theme";
        themeName = "classic";
    }

    QDir      themeDir( mAvailableThemes[ themeName ] );
    QSettings theme( themeDir.filePath( "index.theme" ), QSettings::IniFormat );

    mHeaderFile = themeDir.filePath( theme.value( "Files/Header", QString() ).toString() );
    mBodyFile   = themeDir.filePath( theme.value( "Files/Body", QString() ).toString() );
    mFooterFile = themeDir.filePath( theme.value( "Files/Footer", QString() ).toString() );
    mQssFile    = themeDir.filePath( theme.value( "Files/QSS", QString() ).toString() );

    if ( mHeaderFile.isEmpty() && mFooterFile.isEmpty() ) {
        mSupportsGrouping = false;
    }

    else {
        mSupportsGrouping = theme.value( "Features/SupportsGrouping", false ).toBool();
    }

    mMaxVisibleCount = theme.value( "Features/MaxVisibleCount", 0 ).toUInt();

    mBaseClr   = getColor( theme.value( "Theme/BaseColor" ) );
    mBorderClr = getColor( theme.value( "Theme/BorderColor" ) );

    mBorderRadius = theme.value( "Theme/BorderRadius", 0.0 ).toDouble();
}


bool DesQ::Notifier::ThemeManager::supportsGrouping() {
    return mSupportsGrouping;
}


QVariantMap DesQ::Notifier::ThemeManager::headerLayout( DFL::NotifyWatcher::Notification n ) {
    if ( mHeaderFile.endsWith( ".hjson" ) == false ) {
        return QVariantMap();
    }

    QVariantMap lytMap = DFL::Config::Hjson::readConfigFromFile( mHeaderFile );

    /**
     * Apply theme-specific priority overrides. Components may or may not describe overrides.
     * Overrides will always be property with key "Overrides". This property will describe a
     * map, with keys "Critical", "Normal", and "Tertiary".
     */

    if ( n.hints[ "urgency" ].toInt() == 2 ) {
        lytMap[ "Properties" ] = applyOverrides( lytMap[ "Properties" ].toMap(), "Critical" );
    }

    else if ( n.hints[ "urgency" ].toInt() == 0 ) {
        lytMap[ "Properties" ] = applyOverrides( lytMap[ "Properties" ].toMap(), "Low" );
    }

    else {
        lytMap[ "Properties" ] = applyOverrides( lytMap[ "Properties" ].toMap(), "Normal" );
    }

    return lytMap;
}


QVariantMap DesQ::Notifier::ThemeManager::bodyLayout( DFL::NotifyWatcher::Notification n ) {
    QVariantMap lytMap = DFL::Config::Hjson::readConfigFromFile( mBodyFile );

    /**
     * Apply theme-specific priority overrides. Components may or may not describe overrides.
     * Overrides will always be property with key "Overrides". This property will describe a
     * map, with keys "Critical", "Normal", and "Tertiary".
     */

    if ( n.hints[ "urgency" ].toInt() == 2 ) {
        lytMap[ "Properties" ] = applyOverrides( lytMap[ "Properties" ].toMap(), "Critical" );
    }

    else if ( n.hints[ "urgency" ].toInt() == 0 ) {
        lytMap[ "Properties" ] = applyOverrides( lytMap[ "Properties" ].toMap(), "Low" );
    }

    else {
        lytMap[ "Properties" ] = applyOverrides( lytMap[ "Properties" ].toMap(), "Normal" );
    }

    return lytMap;
}


QVariantMap DesQ::Notifier::ThemeManager::footerLayout( DFL::NotifyWatcher::Notification n ) {
    if ( mFooterFile.endsWith( ".hjson" ) == false ) {
        return QVariantMap();
    }

    QVariantMap lytMap = DFL::Config::Hjson::readConfigFromFile( mFooterFile );

    /**
     * Apply theme-specific priority overrides. Components may or may not describe overrides.
     * Overrides will always be property with key "Overrides". This property will describe a
     * map, with keys "Critical", "Normal", and "Tertiary".
     */

    if ( n.hints[ "urgency" ].toInt() == 2 ) {
        lytMap[ "Properties" ] = applyOverrides( lytMap[ "Properties" ].toMap(), "Critical" );
    }

    else if ( n.hints[ "urgency" ].toInt() == 0 ) {
        lytMap[ "Properties" ] = applyOverrides( lytMap[ "Properties" ].toMap(), "Low" );
    }

    else {
        lytMap[ "Properties" ] = applyOverrides( lytMap[ "Properties" ].toMap(), "Normal" );
    }

    return lytMap;
}


QString DesQ::Notifier::ThemeManager::styleSheet( DFL::NotifyWatcher::Notification ) {
    QString stylesheet;
    QFile   qss( mQssFile );

    if ( qss.open( QFile::ReadOnly ) ) {
        stylesheet = QString::fromUtf8( qss.readAll() );
        qss.close();
    }

    return stylesheet;
}


uint DesQ::Notifier::ThemeManager::maximumVisibleCount() {
    return mMaxVisibleCount;
}


QColor DesQ::Notifier::ThemeManager::baseColor() {
    return mBaseClr;
}


QColor DesQ::Notifier::ThemeManager::borderColor() {
    return mBorderClr;
}


qreal DesQ::Notifier::ThemeManager::borderRadius() {
    return mBorderRadius;
}
