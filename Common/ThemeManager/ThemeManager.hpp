/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "ClientGlobal.hpp"
#include "ServerGlobal.hpp"

#include <DFInotify.hpp>
#include <DFNotifyWatcher.hpp>

namespace DesQ {
    namespace Notifier {
        class ThemeManager;
    }
}

/**
 * A notification can be split into three parts.
 * 1. Header - Optional
 *    App icon, App name etc.
 * 2. Body - Needed
 *    The main notification content.
 * 3. Footer - Options
 *    Can contain auxilliary information:
 *    - No of other notifications
 *    - App name and app icon if there was no title
 *
 * A theme can provide a single layout file, in which case,
 * it will be treated as a "Body", and without grouping support.
 * A theme that provides a "Header" or a "Footer" or both along
 * with "Body" will be, by default considered to support groups.
 */

class DesQ::Notifier::ThemeManager : public QObject {
    Q_OBJECT;

    public:
        ThemeManager();
        ~ThemeManager();

        /**
         * Add a path to global @themePaths variable.
         * If the path exists, the function will update themePaths variable
         * and return true. Otherwise, it will return false.
         * Note: Call this function before creating an instance of this class.
         * Otherwise, themes in the newly added directory will not be listed.
         */
        static bool addThemePath( QString );

        /** List the available themes */
        static QStringList availableThemes();

        /** Theme name */
        QString themeName();

        /** Set the theme name */
        void setThemeName( QString );

        /**
         * Can we merge multiple bubbles from the same application?
         * Themes supporting grouping should provide either a header
         * or a footer layout separate from a body layout.
         */
        bool supportsGrouping();

        /**
         * Maximum number of visible notifications per group.
         * When the number of notifications exceed this value, pause the oldest
         * notification and hide it. When the number of visible notifications come
         * below this value, re-show the last hidden notification. Ensure that
         * re-shown notifications are visible for at least 2 seconds.
         * Note: All critical notifications will always be shown.
         */
        uint maximumVisibleCount();

        /**
         * Header layout for a given priority.
         * Note: The layouts need not be different to different urgencies.
         * Furthermore, if a higher priority notification is shown after a lower
         * one, then the header will be replaced by the higher priority layout.
         */
        QVariantMap headerLayout( DFL::NotifyWatcher::Notification n );

        /**
         * Body layout for a given priority.
         * Note: The layouts need not be different to different urgencies.
         *
         */
        QVariantMap bodyLayout( DFL::NotifyWatcher::Notification n );

        /**
         * Footer layout for a given priority.
         * Note: The layouts need not be different to different urgencies.
         * Unlike the header, the footer will match with the priority of
         * the bottom-most notification.
         */
        QVariantMap footerLayout( DFL::NotifyWatcher::Notification n );

        /**
         * All the widgets can be styled using Qt StyleSheets.
         * It is recommended that the styling is kept to a minimum to retain
         * the Desktop Theme looks.
         * Note: The stylesheets need not be different to different urgencies.
         */
        QString styleSheet( DFL::NotifyWatcher::Notification n );

        /**
         * Base color
         * If defined, the base color is used as the background of the whole bubble.
         * The base is drawn before drawing the widgets.
         */
        QColor baseColor();

        /**
         * Border color
         * If defined, a border is drawn around the whole bubble with the specified color.
         * The border is drawn
         */
        QColor borderColor();

        /**
         * Border radius
         * A rounded rectangle with this radius is used to draw the base and the border.
         */
        qreal borderRadius();

    private:
        QString mThemeName;
        QString mThemeDir;

        QString mHeaderFile;
        QString mBodyFile;
        QString mFooterFile;
        QString mQssFile;

        bool mSupportsGrouping = false;

        uint mMaxVisibleCount = 0;

        QColor mBaseClr;
        QColor mBorderClr;
        qreal mBorderRadius = 0.0;

        static QMap<QString, QString> mAvailableThemes;

        DFL::Inotify *fsWatcher = nullptr;
};
