/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ThemeUtils.hpp"

QVariantMap applyOverrides( QVariantMap props, QString priority ) {
    QStringList items = props.keys();

    for ( QString item: items ) {
        QVariantMap itemProps = props[ item ].toMap();

        /** No overrides defined. Let's go to the next item */
        if ( itemProps.contains( "Overrides" ) == false ) {
            continue;
        }

        /** We don't have an override that is specific to out priority */
        if ( itemProps[ "Overrides" ].toMap().contains( priority ) == false ) {
            continue;
        }

        QVariantMap overrides = itemProps[ "Overrides" ].toMap().value( priority ).toMap();
        for ( QString oKey: overrides.keys() ) {
            itemProps[ oKey ] = overrides[ oKey ];
        }

        props[ item ] = itemProps;
    }

    return props;
}
