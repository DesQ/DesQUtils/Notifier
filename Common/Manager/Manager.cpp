/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Manager.hpp"

DesQ::Notifier::Manager::Manager( QWidget *parent ): QWidget( parent ) {
    createUI();
}


void DesQ::Notifier::Manager::createUI() {
    filterBtn = new QLabel();
    filterLE  = new QLineEdit();

    QHBoxLayout *filterLyt = new QHBoxLayout();
    filterLyt->addStretch();
    filterLyt->addWidget( filterBtn );
    filterLyt->addWidget( filterLE );
    filterLyt->addStretch();

    activeScroll = new QScrollArea();
    activeScroll->setContentsMargins( QMargins() );

    inactiveScroll = new QScrollArea();
    inactiveScroll->setContentsMargins( QMargins() );

    tabs = new QTabWidget();
    tabs->addTab( activeScroll,   "Active" );
    tabs->addTab( inactiveScroll, "Archived" );

    baseLyt = new QVBoxLayout();
    baseLyt->setContentsMargins( QMargins() );
    baseLyt->addWidget( filterLyt );
    baseLyt->addStretch();

    setLayout( baseLyt );
}
