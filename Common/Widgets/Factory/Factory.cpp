/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Factory.hpp"

#include "Labels.hpp"
#include "Buttons.hpp"
#include "LineEdit.hpp"
#include "ProgressBar.hpp"


DesQ::Notifier::WidgetFactory::WidgetFactory( QSize referenceSize ): DesQUI::WidgetFactory( referenceSize ) {
    //
}


void DesQ::Notifier::WidgetFactory::setNotification( DFL::NotifyWatcher::Notification *n ) {
    mN = n;
}


QWidget * DesQ::Notifier::WidgetFactory::createWidget( QString widget, QString type, QVariantMap props ) {
    if ( isWidgetNeeded( widget, type ) == false ) {
        return nullptr;
    }

    if ( props.contains( "Width" ) and props[ "Width" ].userType() == QMetaType::Double ) {
        props[ "Width" ] = round( props[ "Width" ].toDouble() * mRefSize.width() );
    }

    if ( props.contains( "Height" ) and props[ "Height" ].userType() == QMetaType::Double ) {
        props[ "Height" ] = round( props[ "Height" ].toDouble() * mRefSize.width() );
    }

    if ( props.contains( "MinWidth" ) and props[ "MinWidth" ].userType() == QMetaType::Double ) {
        props[ "MinWidth" ] = round( props[ "MinWidth" ].toDouble() * mRefSize.width() );
    }

    if ( props.contains( "MinHeight" ) and props[ "MinHeight" ].userType() == QMetaType::Double ) {
        props[ "MinHeight" ] = round( props[ "MinHeight" ].toDouble() * mRefSize.width() );
    }

    if ( widget == "NotifyIcon" ) {
        NotifyIcon *icon = new NotifyIcon( widget, type, props );
        icon->setAttribute( Qt::WA_TransparentForMouseEvents );
        return icon;
    }

    if ( widget == "NotifyCount" ) {
        NotifyCount *count = new NotifyCount( widget, props );
        count->setAttribute( Qt::WA_TransparentForMouseEvents );
        return count;
    }

    if ( widget == "AppIcon" ) {
        props[ "Name" ] = mN->appIcon;
        AppIcon *icon = new AppIcon( widget, props );
        icon->setAttribute( Qt::WA_TransparentForMouseEvents );
        return icon;
    }

    if ( widget == "AppName" ) {
        props[ "Text" ] = mN->appName;
        AppName *lbl = new AppName( widget, props );
        lbl->setAttribute( Qt::WA_TransparentForMouseEvents );
        return lbl;
    }

    if ( widget == "CloseButton" ) {
        CloseButton *btn = new CloseButton( widget, props );
        return btn;
    }

    if ( widget == "Summary" ) {
        props[ "Text" ] = mN->summary;
        Summary *lbl = new Summary( widget, props );
        lbl->setAttribute( Qt::WA_TransparentForMouseEvents );
        return lbl;
    }

    if ( widget == "Body" ) {
        props[ "Text" ] = mN->body;
        Body *lbl = new Body( widget, props );
        lbl->setAttribute( Qt::WA_TransparentForMouseEvents );
        return lbl;
    }

    if ( widget == "ActionButton" ) {
        if ( type.toInt() > 6 ) {
            qCritical() << "Not supporting more than 6 actions at present. Not adding" << mN->actions[ type.toInt() - 1 ];
            return nullptr;
        }

        QStringList parts = mN->actions[ type.toInt() - 1 ].split( ":" );
        props[ "Text" ]   = parts.at( 0 );
        props[ "Action" ] = parts.at( 1 );

        ActionButton *btn = new ActionButton( widget, type, props );
        return btn;
    }

    if ( widget == "ReplyEdit" ) {
        ReplyEdit *edit = new ReplyEdit( widget, props );
        return edit;
    }

    if ( widget == "ReplyButton" ) {
        ReplyButton *btn = new ReplyButton( widget, props );
        return btn;
    }

    if ( widget == "ProgressBar" ) {
        ProgressBar *pBar = new ProgressBar( widget, props );
        pBar->setAttribute( Qt::WA_TransparentForMouseEvents );
        return pBar;
    }

    else {
        return nullptr;
    }

    return nullptr;
}


QStringList DesQ::Notifier::WidgetFactory::availableWidgets() {
    return {
        "ActionButton",
        "AppIcon",
        "AppName",
        "Body",
        "CloseButton",
        "NotifyCount",
        "NotifyIcon",
        "ProgressBar",
        "Summary",
    };
}


QStringList DesQ::Notifier::WidgetFactory::availableTypes( QString widget ) {
    if ( widget == "NotifyIcon" ) {
        return { "Info", "Warning", "Question", "Other" };
    }

    return { "Default" };
}


QStringList DesQ::Notifier::WidgetFactory::availableProperties( QString widget, QString ) {
    QStringList properties;

    properties << "Width" << "Height" << "MinWidth" << "MinHeight";

    if ( widget == "NotifyIcon" ) {
        properties << "Name" << "Size" << "Type";
    }

    else if ( widget == "NotifyCount" ) {
        properties << "Width" << "Height" << "Text";
    }

    else if ( widget == "AppIcon" ) {
        properties << "Name" << "Size";
    }

    else if ( widget == "AppName" ) {
        properties << "Text";
    }

    else if ( widget == "CloseButton" ) {
        properties << "Icon";
    }

    else if ( widget == "Summary" ) {
        properties << "Text" << "Margins";
    }

    else if ( widget == "Body" ) {
        properties << "Text" << "Margins";
    }

    else if ( widget == "ActionButton" ) {
        properties << "Text";
    }

    else if ( widget == "ProgressBar" ) {
        properties << "Value" << "Minimum" << "Maximum";
    }

    else {
        return QStringList();
    }

    return properties;
}


bool DesQ::Notifier::WidgetFactory::isWidgetNeeded( QString widget, QString type ) {
    /** We do not have any restrictions on AppIcon */
    if ( widget == "AppIcon" ) {
        return true;
    }

    /** We do not have any restrictions on NotifyIcon */
    if ( widget == "NotifyIcon" ) {
        return true;
    }

    /** We do not have any restrictions on NotifyCount */
    if ( widget == "NotifyCount" ) {
        return true;
    }

    /** We do not have any restrictions on NotifyIcon */
    if ( widget == "AppName" ) {
        return true;
    }

    /** We do not have any restrictions on NotifyIcon */
    if ( widget == "CloseButton" ) {
        return true;
    }

    /** We do not have any restrictions on NotifyIcon */
    if ( widget == "Summary" ) {
        return ( mN->summary.isEmpty() == false );
    }

    /** We do not have any restrictions on NotifyIcon */
    if ( widget == "Body" ) {
        return ( mN->body.isEmpty() == false );
    }

    /** We do not have any restrictions on NotifyIcon */
    if ( widget == "ActionButton" ) {
        return ( mN->actions.count() >= type.toInt() );
    }

    /** We do not have any restrictions on NotifyIcon */
    if ( widget == "ProgressBar" ) {
        return mN->hints.contains( "percent" );
    }

    return false;
}
