/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "ClientGlobal.hpp"
#include "ServerGlobal.hpp"

#include <desqui/LayoutManager.hpp>
#include <DFNotifyWatcher.hpp>

namespace DesQ {
    namespace Notifier {
        class WidgetFactory;
    }
}

/**
 * Widget Factory for DesQ Notifier.
 * This class is responsible for creating widgets and applying various properties.
 */
class DesQ::Notifier::WidgetFactory : public DesQUI::WidgetFactory {
    public:
        WidgetFactory( QSize referenceSize );
        ~WidgetFactory() = default;

        /** Set the notification. Widgets will be created based on the notification */
        void setNotification( DFL::NotifyWatcher::Notification *n );

        /** Return a valid widget only if its needed. Otherwise, return a nullptr */
        QWidget * createWidget( QString widget, QString type, QVariantMap props );

        /** Names of the available widgets. The names need not be Qt class names */
        QStringList availableWidgets();

        /** Types available for the given widget. */
        QStringList availableTypes( QString widget );

        /** Properties that can be set for the given widget of the given type */
        QStringList availableProperties( QString widget, QString type );

    private:
        DFL::NotifyWatcher::Notification *mN;

        bool isWidgetNeeded( QString widget, QString type );
};
