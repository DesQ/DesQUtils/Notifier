/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Misc.hpp"
#include "Labels.hpp"

NotifyIcon::NotifyIcon( QString widget, QString type, QVariantMap props ) {
    setObjectName( widget );
    setAlignment( Qt::AlignCenter );
    setProperty( "Type", type );

    if ( props.contains( "Width" ) ) {
        setFixedWidth( props[ "Width" ].toInt() );
    }

    if ( props.contains( "Height" ) ) {
        setFixedHeight( props[ "Height" ].toInt() );
    }

    if ( props.contains( "MinWidth" ) ) {
        setMinimumWidth( props[ "MinWidth" ].toInt() );
    }

    if ( props.contains( "MinHeight" ) ) {
        setMinimumHeight( props[ "MinHeight" ].toInt() );
    }

    setPixmap( QIcon::fromTheme( props[ "Name" ].toString() ).pixmap( props[ "Size" ].toInt() ) );
}


AppIcon::AppIcon( QString widget, QVariantMap props ) {
    setObjectName( widget );
    setAlignment( Qt::AlignCenter );

    if ( props.contains( "Width" ) ) {
        setFixedWidth( props[ "Width" ].toInt() );
    }

    if ( props.contains( "Height" ) ) {
        setFixedHeight( props[ "Height" ].toInt() );
    }

    if ( props.contains( "MinWidth" ) ) {
        setMinimumWidth( props[ "MinWidth" ].toInt() );
    }

    if ( props.contains( "MinHeight" ) ) {
        setMinimumHeight( props[ "MinHeight" ].toInt() );
    }

    /** We're sure to have an icon by now. */
    setPixmap( props[ "QIcon" ].value<QIcon>().pixmap( props[ "Size" ].toInt() ) );
}


AppName::AppName( QString widget, QVariantMap props ) {
    setObjectName( widget );

    /** Expand along the horizontal direction */
    setSizePolicy( QSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed ) );

    if ( props.contains( "Width" ) ) {
        setFixedWidth( props[ "Width" ].toInt() );
    }

    if ( props.contains( "Height" ) ) {
        setFixedHeight( props[ "Height" ].toInt() );
    }

    if ( props.contains( "MinWidth" ) ) {
        setMinimumWidth( props[ "MinWidth" ].toInt() );
    }

    if ( props.contains( "MinHeight" ) ) {
        setMinimumHeight( props[ "MinHeight" ].toInt() );
    }

    setText( props[ "Text" ].toString() );
}


Summary::Summary( QString widget, QVariantMap props ) {
    setObjectName( widget );
    setWordWrap( true );

    /** Expand along the horizontal direction */
    setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Fixed ) );

    if ( props.contains( "Width" ) ) {
        setFixedWidth( props[ "Width" ].toInt() );
    }

    if ( props.contains( "Height" ) ) {
        setFixedHeight( props[ "Height" ].toInt() );
    }

    if ( props.contains( "MinWidth" ) ) {
        setMinimumWidth( props[ "MinWidth" ].toInt() );
    }

    if ( props.contains( "MinHeight" ) ) {
        setMinimumHeight( props[ "MinHeight" ].toInt() );
    }

    int m = props[ "Margins" ].toInt();
    setContentsMargins( QMargins( m, m, m, m ) );

    setText( "<b>" + props[ "Text" ].toString() + "</b>" );
}


Body::Body( QString widget, QVariantMap props ) {
    setObjectName( widget );
    setWordWrap( true );

    /** Expand along the horizontal direction */
    setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Fixed ) );

    if ( props.contains( "Width" ) ) {
        setFixedWidth( props[ "Width" ].toInt() );
    }

    if ( props.contains( "Height" ) ) {
        setFixedHeight( props[ "Height" ].toInt() );
    }

    if ( props.contains( "MinWidth" ) ) {
        setMinimumWidth( props[ "MinWidth" ].toInt() );
    }

    if ( props.contains( "MinHeight" ) ) {
        setMinimumHeight( props[ "MinHeight" ].toInt() );
    }

    int m = props[ "Margins" ].toInt();
    setContentsMargins( QMargins( m, m, m, m ) );

    setText( props[ "Text" ].toString() );
}


NotifyCount::NotifyCount( QString widget, QVariantMap props ) {
    setObjectName( widget );

    /** Expand along the horizontal direction */
    setSizePolicy( QSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed ) );

    if ( props.contains( "Width" ) ) {
        setFixedWidth( props[ "Width" ].toInt() );
    }

    if ( props.contains( "Height" ) ) {
        setFixedHeight( props[ "Height" ].toInt() );
    }

    if ( props.contains( "MinWidth" ) ) {
        setMinimumWidth( props[ "MinWidth" ].toInt() );
    }

    if ( props.contains( "MinHeight" ) ) {
        setMinimumHeight( props[ "MinHeight" ].toInt() );
    }

    setText( props[ "Text" ].toString() );
}
