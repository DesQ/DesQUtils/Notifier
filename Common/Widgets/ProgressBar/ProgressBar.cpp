/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ProgressBar.hpp"


ProgressBar::ProgressBar( QString widget, QVariantMap props ) {
    setObjectName( widget );

    setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Fixed ) );

    if ( props.contains( "Width" ) ) {
        setFixedWidth( props[ "Width" ].toInt() );
    }

    if ( props.contains( "Height" ) ) {
        setFixedHeight( props[ "Height" ].toInt() );
    }

    if ( props.contains( "MinWidth" ) ) {
        setMinimumWidth( props[ "MinWidth" ].toInt() );
    }

    if ( props.contains( "MinHeight" ) ) {
        setMinimumHeight( props[ "MinHeight" ].toInt() );
    }

    /** Set the range */
    setRange( props[ "Minimum" ].toInt(), props[ "Maximum" ].toInt() );

    /** Set the value */
    setValue( props[ "Value" ].toInt() );
}
