/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Buttons.hpp"

CloseButton::CloseButton( QString widget, QVariantMap props ) : QToolButton() {
    setObjectName( widget );
    setAutoRaise( true );

    setIcon( QIcon::fromTheme( props[ "Icon" ].toString(), QIcon( props[ "Icon" ].toString() ) ) );

    if ( props.contains( "Size" ) ) {
        int icoSize = props[ "Size" ].toInt();
        setIconSize( QSize( icoSize, icoSize ) );
    }

    if ( props.contains( "Width" ) ) {
        setFixedWidth( props[ "Width" ].toInt() );
    }

    if ( props.contains( "Height" ) ) {
        setFixedHeight( props[ "Height" ].toInt() );
    }
}


ActionButton::ActionButton( QString widget, QString type, QVariantMap props ) : QPushButton() {
    setObjectName( widget );
    setFlat( true );

    setText( props[ "Text" ].toString() );
    setProperty( "Action", props[ "Action" ].toString() );
    setProperty( "Type",   type );

    if ( props.contains( "Width" ) ) {
        setFixedWidth( props[ "Width" ].toInt() );
    }

    if ( props.contains( "Height" ) ) {
        setFixedHeight( props[ "Height" ].toInt() );
    }
}


ReplyButton::ReplyButton( QString widget, QVariantMap props ) : QToolButton() {
    setObjectName( widget );

    setIcon( QIcon::fromTheme( "document-send" ) );

    if ( props.contains( "Width" ) ) {
        setFixedWidth( props[ "Width" ].toInt() );
    }

    if ( props.contains( "Height" ) ) {
        setFixedHeight( props[ "Height" ].toInt() );
    }
}
