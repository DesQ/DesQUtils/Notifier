/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <iostream>
#include <signal.h>

#include <QtDBus>
#include <QCommandLineParser>

#include <DFNotifyWatcher.hpp>
#include <DFCoreApplication.hpp>

#include "ClientGlobal.hpp"
#include "MainClient.hpp"

extern void showHelp();
extern void createCLI( QCommandLineParser * );

DesQ::Notifier::DBusClient::DBusClient() {
    int ret = QDBusConnection::sessionBus().connect(
        "org.freedesktop.Notifications",
        "/org/freedesktop/Notifications",
        "org.freedesktop.Notifications",
        "ActionInvoked",
        "us",
        this,
        SLOT( onActionInvoked( uint, QString ) )
    );

    if ( ret == false ) {
        qDebug() << "Failed to connect to D-Bus signal 'ActionInvoked'.";
    }

    ret = QDBusConnection::sessionBus().connect(
        "org.freedesktop.Notifications",
        "/org/freedesktop/Notifications",
        "org.freedesktop.Notifications",
        "NotificationClosed",
        "uu",
        this,
        SLOT( onNotificationClosed( uint, uint ) )
    );

    if ( ret == false ) {
        qDebug() << "Failed to connect to D-Bus signal 'NotificationClosed'.";
    }

    ret = QDBusConnection::sessionBus().connect(
        "org.freedesktop.Notifications",
        "/org/freedesktop/Notifications",
        "org.freedesktop.Notifications",
        "NotificationReplied",
        "us",
        this,
        SLOT( onNotificationReplied( uint, QString ) )
    );

    if ( ret == false ) {
        qDebug() << "Failed to connect to D-Bus signal 'NotificationReplied'.";
    }
}


void DesQ::Notifier::DBusClient::sendDBusMessage( QCommandLineParser *parser ) {
    /** Assuming that a notification daemon is running */
    QDBusInterface iface(
        "org.freedesktop.Notifications",
        "/org/freedesktop/Notifications",
        "org.freedesktop.Notifications",
        QDBusConnection::sessionBus()
    );

    /**Check if a close was requested */
    if ( parser->isSet( "close" ) ) {
        QDBusReply<uint> reply = iface.call(
            QDBus::Block,
            "CloseNotification",
            parser->value( "close" ).toUInt()
        );

        if ( reply.isValid() == false ) {
            qWarning() << "Failed to close notification:" << reply.error().message();
        }

        /** We're done here!! */
        return;
    }

    QStringList posArgs = parser->positionalArguments();

    QVariantMap hints;

    if ( parser->isSet( "hint" ) ) {
        QStringList rawHints = parser->values( "hint" );
        for ( QString rawHint: rawHints ) {
            QStringList hintBits = rawHint.simplified().split( ":" );

            if ( hintBits.count() != 3 ) {
                continue;
            }

            if ( allowedHints.contains( hintBits.at( 1 ).toLower() + ":" + hintBits.at( 0 ).toLower() ) == false ) {
                qWarning() << "Unknown hint:" << QString( "%1 of type %2" ).arg( hintBits.at( 1 ) ).arg( hintBits.at( 0 ) );
                qWarning() << "  Allowed types are:" << allowedHints;
                continue;
            }

            QString hint = hintBits.at( 1 ).toLower();

            if ( hint == "urgency" ) {
                QString urgency = hintBits.at( 2 );

                if ( ( urgency == "0" ) or ( urgency == "low" ) ) {
                    hints[ "urgency" ] = 0;
                }

                else if ( ( urgency == "1" ) or ( urgency == "normal" ) ) {
                    hints[ "urgency" ] = 1;
                }

                else if ( ( urgency == "2" ) or ( urgency == "critical" ) ) {
                    hints[ "urgency" ] = 2;
                }

                else {
                    qWarning() << "Invalid values for 'urgency'" << urgency;
                    qWarning() << "  Choose one of" << QStringList( { "low", "normal", "critical" } ) << "or" << "(0, 1, 2)";
                    hints[ "urgency" ] = 1;
                }
            }

            else if ( hint == "transient" ) {
                QString value = hintBits.at( 2 ).toLower();

                if ( not QStringList( { "true", "false" } ).contains( value ) ) {
                    qWarning() << "Invalid value for 'transient' '" + value + "'. Treating as false.";
                }

                if ( value == "true" ) {
                    hints[ "transient" ] = true;
                }

                else {
                    hints[ "transient" ] = false;
                }
            }

            else if ( hint == "resident" ) {
                QString value = hintBits.at( 2 ).toLower();

                if ( not QStringList( { "true", "false" } ).contains( value ) ) {
                    qWarning() << "Invalid value for 'resident' '" + value + "'. Treating as false.";
                }

                if ( value == "true" ) {
                    hints[ "resident" ] = true;
                }

                else {
                    hints[ "resident" ] = false;
                }
            }

            else if ( hint == "percent" ) {
                bool ok;
                hints[ "percent" ] = hintBits.at( 2 ).toDouble( &ok );

                if ( not ok ) {
                    qWarning() << "Cannot interpret" << hintBits.at( 2 ) << "as double. Treated as 0";
                }
            }

            else {
                hints[ hint ] = hintBits.at( 2 );
            }
        }
    }

    /** Parse the replace request id */
    uint request_id = 0;

    if ( parser->isSet( "replace-id" ) ) {
        bool ok;
        request_id = parser->value( "replace-id" ).toUInt( &ok );

        if ( not ok ) {
            qWarning()
                << "Cannot interpret replace-id" << parser->value( "replace-id" )
                << "as base-10 int. Treated as 0.";
        }
    }

    /** Parse the timeout */
    int timeout = -1;

    if ( parser->isSet( "expire-time" ) ) {
        bool ok;
        timeout = parser->value( "expire-time" ).toInt( &ok );

        if ( not ok ) {
            qWarning()
                << "Cannot interpret expire-timeout" << parser->value( "expire-time" )
                << "as base-10 int. Using the default timeout.";

            timeout = -1;
        }
    }

    /** Set a hint if we have urgency set */
    if ( parser->isSet( "urgency" ) ) {
        QString urgency = parser->value( "urgency" );

        if ( ( urgency == "0" ) or ( urgency == "low" ) ) {
            hints[ "urgency" ] = 0;
        }

        else if ( ( urgency == "1" ) or ( urgency == "normal" ) ) {
            hints[ "urgency" ] = 1;
        }

        else if ( ( urgency == "2" ) or ( urgency == "critical" ) ) {
            hints[ "urgency" ] = 2;
        }

        else {
            qWarning() << "Invalid values for 'urgency'" << urgency;
            qWarning() << "  Choose one of" << QStringList( { "low", "normal", "critical" } ) << "or" << "(0, 1, 2)";
            hints[ "urgency" ] = 1;
        }
    }

    /** Otherwise default to normal */
    else {
        hints[ "urgency" ] = 1;
    }

    QStringList actions;

    /**
     * First we check for ":" as the separator.
     * Then we check for "=" as the separator.
     */
    for ( QString action: parser->values( "action" ) ) {
        /** desq-notifier style */
        if ( action.contains( ":" ) ) {
            actions << action.split( ":" );
        }

        /** notify-send support */
        else if ( action.contains( "=" ) ) {
            actions << action.split( "=" );
        }
    }

    DFL::NotifyWatcher::Notification n {
        /** Application name - useful for grouping notifications */
        ( parser->isSet( "app-name" )   ? parser->value( "app-name" )                   : "Unknown" ),
        /** Used for replacing notificaitons - hard to use from CLI */
        request_id,
        /** Notification icon - if not given, we will use a custom icon */
        ( parser->isSet( "icon" )       ? parser->value( "icon" )                       : "dialog-information" ),
        /** Summary - used as notification title */
        ( posArgs.count() >= 1          ? posArgs.value( 0 ).replace( "\n", "<br>" )    : "" ),
        /** Body - if you feel like telling more than 5 words of the title */
        ( posArgs.count() >= 2          ? posArgs.value( 1 ).replace( "\n", "<br>" )    : "" ),
        /** Actions - if you want a way to interact with the sender */
        ( parser->isSet( "action" )     ? actions                                       : QStringList() ),
        /** Special hints - desktop entry, icon path, sound, blah blah blah */
        hints,
        /** Timeout - how long (ms) should we show this notification; 0 - forever; 5000 - default */
        timeout,
    };

    /** Send the  */
    QDBusReply<uint> reply = iface.call(
        QDBus::Block,
        "Notify",
        n.appName,
        n.id,
        n.appIcon,
        n.summary,
        n.body,
        n.actions,
        n.hints,
        n.timeout
    );

    /** Retrieve the notification ID */
    mNotifyId = reply.value();

    /** Notification failed: Exit with error */
    if ( mNotifyId == 0 ) {
        std::exit( 1 );
    }

    if ( parser->isSet( "print-id" ) ) {
        std::cout << mNotifyId << std::endl;

        /** If wait is not set and there are no actions, we can exit */
        if ( ( parser->isSet( "wait" ) == false ) and ( parser->isSet( "action" ) == false ) ) {
            std::exit( 0 );
        }
    }
}


void DesQ::Notifier::DBusClient::onActionInvoked( uint id, QString action ) {
    if ( id == mNotifyId ) {
        std::cout << action.toStdString() << std::endl;
    }
}


void DesQ::Notifier::DBusClient::onNotificationClosed( uint id, uint reason ) {
    if ( id == mNotifyId ) {
        std::cout << reason << std::endl;

        /** Once the notification is closed, we will exit */
        std::exit( 0 );
    }
}


void DesQ::Notifier::DBusClient::onNotificationReplied( uint id, QString replyText ) {
    if ( id == mNotifyId ) {
        std::cout << id << " " << replyText.toStdString() << std::endl;
    }
}


int runClientCode( int argc, char *argv[], QCommandLineParser *cliParser ) {
    DFL::CoreApplication app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Notifier" );
    app.setApplicationVersion( PROJECT_VERSION );

    app.interceptSignal( SIGINT,  true );
    app.interceptSignal( SIGTERM, true );
    app.interceptSignal( SIGQUIT, true );
    app.interceptSignal( SIGABRT, true );
    app.interceptSignal( SIGSEGV, true );

    DesQ::Notifier::DBusClient client;

    /**
     * Error checking
     * 1. Check if -x|--close is set
     */
    if ( cliParser->isSet( "close" ) ) {
        /** Check if at least one value has been set */
        int closeArgsCount = cliParser->values( "close" ).count();

        if ( closeArgsCount ) {
            bool ok;
            uint id = cliParser->value( "close" ).toUInt( &ok );

            if ( ( ok == false ) || ( id == 0 ) ) {
                std::cerr << "Invalid notification ID given." << std::endl;
                return 1;
            }
        }

        else {
            std::cerr << "Please specify the notification ID to be closed." << std::endl;
            return 1;
        }
    }

    if ( cliParser->isSet( "replace-id" ) ) {
        /** Check if at least one value has been set */
        int argsCount = cliParser->values( "replace-id" ).count();

        if ( argsCount ) {
            bool ok;
            uint id = cliParser->value( "replace-id" ).toUInt( &ok );

            if ( ( ok == false ) || ( id == 0 ) ) {
                std::cerr << "Invalid notification ID given." << std::endl;
                return 1;
            }
        }

        else {
            std::cerr << "Please specify the notification ID to be replaced." << std::endl;
            return 1;
        }
    }

    if ( cliParser->isSet( "expire-time" ) ) {
        /** Check if at least one value has been set */
        int argsCount = cliParser->values( "expire-time" ).count();

        if ( argsCount ) {
            bool ok;
            uint id = cliParser->value( "expire-time" ).toUInt( &ok );

            if ( ( ok == false ) || ( id == 0 ) ) {
                std::cerr << "Invalid expiry time." << std::endl;
                return 1;
            }
        }

        else {
            std::cerr << "Please specify the expiry time." << std::endl;
            return 1;
        }
    }

    if ( cliParser->isSet( "action" ) ) {
        /** Check if at least one value has been set */
        int argsCount = cliParser->values( "action" ).count();

        if ( argsCount == 0 ) {
            std::cerr << "This option requires an argument." << std::endl;
            return 1;
        }
    }

    if ( cliParser->isSet( "app-name" ) ) {
        /** Check if at least one value has been set */
        int argsCount = cliParser->values( "app-name" ).count();

        if ( argsCount == 0 ) {
            std::cerr << "This option requires an argument." << std::endl;
            return 1;
        }
    }

    if ( cliParser->isSet( "icon" ) ) {
        /** Check if at least one value has been set */
        int argsCount = cliParser->values( "icon" ).count();

        if ( argsCount == 0 ) {
            std::cerr << "This option requires an argument." << std::endl;
            return 1;
        }
    }

    if ( cliParser->isSet( "urgency" ) ) {
        /** Check if at least one value has been set */
        int argsCount = cliParser->values( "urgency" ).count();

        if ( argsCount ) {
            bool    ok;
            QString urgencyStr = cliParser->value( "urgency" );
            uint    urgency    = urgencyStr.toUInt( &ok );

            if ( ( ( ok == false ) && urgencyStr.isEmpty() ) || ( urgency > 2 ) ) {
                std::cerr << "Invalid value passed to this option." << std::endl;
                return 1;
            }
        }

        else {
            std::cerr << "Please specify the urgency level" << std::endl;
            return 1;
        }
    }

    if ( cliParser->isSet( "category" ) ) {
        /** Check if at least one value has been set */
        int argsCount = cliParser->values( "category" ).count();

        if ( argsCount == 0 ) {
            std::cerr << "This option requires an argument." << std::endl;
            return 1;
        }
    }

    if ( cliParser->isSet( "hint" ) ) {
        /** Check if at least one value has been set */
        int argsCount = cliParser->values( "hint" ).count();

        if ( argsCount == 0 ) {
            std::cerr << "This option requires an argument." << std::endl;
            return 1;
        }
    }

    if ( cliParser->positionalArguments().count() < 2 ) {
        std::cerr << "Please specify the summary and body of this notification." << std::endl;
        return 1;
    }

    client.sendDBusMessage( cliParser );

    /** If print-id/wait/action are set, we should wait. */
    bool shouldWait = cliParser->isSet( "print-id" );
    shouldWait |= cliParser->isSet( "wait" );
    shouldWait |= cliParser->isSet( "action" );

    /** Also, we should not be closing a notification */
    shouldWait &= ( cliParser->isSet( "close" ) == false );

    if ( shouldWait ) {
        qDebug() << "Waiting";
        return app.exec();
    }

    return 0;
}
