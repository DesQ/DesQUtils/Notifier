/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>
#include <QString>
#include <QCommandLineParser>

namespace DesQ {
    namespace Notifier {
        class DBusClient;
    }
}

namespace DFL {
    class CoreApplication;
}

class QCommandLineParser;

class DesQ::Notifier::DBusClient : public QObject {
    Q_OBJECT;

    public:
        DBusClient();

        /** Convert cli args into a notification request */
        void sendDBusMessage( QCommandLineParser *parser );

        /** Write the action to the stdout */
        Q_SLOT void onActionInvoked( uint id, QString action );

        /** Close the desq-notifier client instance */
        Q_SLOT void onNotificationClosed( uint id, uint reason );

        /** Write the reply to the stdout */
        Q_SLOT void onNotificationReplied( uint id, QString replyText );

    private:
        uint mNotifyId = 0;
};

int runClientCode( int argc, char *argv[], QCommandLineParser * );
