# DesQ Notifier
## A simple notification server and client for DesQ.

DesQ Notifier is a light-weight, highly themable notification server. It currently supports [several features](#server-features).
DesQ Notifier can also be used as a drop-in replacement for notify-send, and has feature-parity with it. Any deviation is to be treated as a bug.


### Server Features
Currently supported features are
* Grouping of notifications
* Server Capabilities: body, body-hyperlinks, body-images, body-markup, icon-static, persistence
* Hints: desktop-entry, image-path, resident, transient, urgency.
* Additional Hints:
  - percent for progress bar display (type: double)
* Inline reply (like in Android, requires support from client apps)


### Theming support
DesQ Notifier comes with three inbuilt themes: classic, neoclassic (default) and modern. It also supports custom themes  designed by the user.
Themes have the freedom to decide if grouping and hiding of notifications is supported. The NeoClassic and the Modern themes supports both, while
the Classic theme supports neither.


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQUtils/Notifier.git DesQNotifier`
- Enter the `DesQNotifier` folder
  * `cd DesQNotifier`
- Configure the project - we use meson for project management
  * `meson setup .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Dependencies:
* Qt5 or Qt6
* [WayQt](https://gitlab.com/desktop-frameworks/wayqt)
* [DFL::Applications](https://gitlab.com/desktop-frameworks/applications)
* [DFL::Inotify](https://gitlab.com/desktop-frameworks/inotify)
* [DFL::Settings](https://gitlab.com/desktop-frameworks/settings)
* [DFL::Utils](https://gitlab.com/desktop-frameworks/utils)
* [DFL::Notify](https://gitlab.com/desktop-frameworks/notification)
* [DFL::Xdg](https://gitlab.com/desktop-frameworks/xdg)
* [DFL::ConfigParser (Hjson)](https://gitlab.com/desktop-frameworks/configparser)
* [LibDesQ](https://gitlab.com/DesQ/libdesq)
* [LibDesQUI](https://gitlab.com/DesQ/libdesqui)


### Known Bugs
* Notification bubble size is incorrect in Qt6 build.


### Upcoming
* Support for action icons
* Support for activation tokens
* Support for Animated icon (icon-multi)


### TODO
* Notification Manager UI - Window to show all notifications including hidden notifications.
* Settings to mute selected notifications.
* Settings to treat higher priority notifications as lower priority on a per-app basis.
* Do not disturb mode.
* Cache notifications.
* Support for sounds in notifications based on category, and
* Support to mute sounds on a per-app basis.
