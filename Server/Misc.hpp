/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "ClientGlobal.hpp"
#include "ServerGlobal.hpp"

#include <DFNotifyWatcher.hpp>

/** Show help on the cli */
void showHelp();

/** Show version on the cli */
void showVersion();

/** Create the CLI */
void createCLI( QCommandLineParser *parser );

/** Get the server information */
QStringList getServerInformation();

/** Get the server capabilities */
QStringList getServerCapabilities();

/** Utility function to convert CLI opts to QString */
QString cliOptsToString( QCommandLineParser *parser );

/** Check if the notifications service is running */
bool isServiceRunning();
