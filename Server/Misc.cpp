/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ClientGlobal.hpp"
#include "ServerGlobal.hpp"

#include "Misc.hpp"

#include <DFXdg.hpp>
#include <iostream>


void showHelp() {
    std::cout << "DesQ Notifier " PROJECT_VERSION "\n" << std::endl;
    std::cout << "Usage: desq-notifier [options] <summary> [BODY..]\n" << std::endl;

    std::cout << "Arguments:" << std::endl;
    std::cout << "  summary                        Summary of the notification." << std::endl;
    std::cout << "  body                           Body of the notification.\n" << std::endl;

    std::cout << "Application Options:" << std::endl;
    std::cout << "  -A|--action=ACTION=NAME          Specifies an action the user can use to interact with the client" << std::endl;
    std::cout << "  -a|--app-name=APP_NAME           Specifies the app name for the icon." << std::endl;
    std::cout << "  -b|--capabilities                Print the server capabilities." << std::endl;
    std::cout << "  -c|--category=TYPE[,TYPE...]     Specifies the notification category." << std::endl;
    std::cout << "  -x|--close=ID                    Requests the notification server to close the notification ID." << std::endl;
    std::cout << "  -t|--expire-time=TIME            Specifies the timeout in milliseconds at which to expire the notification." << std::endl;
    std::cout << "  -h|--hint=TYPE:NAME:VALUE        Specifies basic extra data to pass. Valid types are int, double, string and byte." << std::endl;
    std::cout << "  -i|--icon=ICON                   Specifies an icon filename or stock icon to display." << std::endl;
    std::cout << "  -p|--print-id                    Print the notification ID." << std::endl;
    std::cout << "  -r|--replace-id=ID               The notification with id that needs to be replaced with this one." << std::endl;
    std::cout << "  -f|--server-info                 Print the server information." << std::endl;
    std::cout << "  -e|--transient                   Create a transient notification." << std::endl;
    std::cout << "  -u|--urgency=LEVEL               Specifies the urgency level (low, normal, critical or 0, 1, 2)." << std::endl;
    std::cout << "  -w|--wait                        Wait for the notification to be closed or user interaction.\n" << std::endl;

    std::cout << "Other Options:" << std::endl;
    std::cout << "  -?|--help                        Print this help and exit." << std::endl;
    std::cout << "  -v|--version                     Print application version and exit." << std::endl;
}


void showVersion() {
    std::cout << "DesQ Notifier " PROJECT_VERSION << std::endl;
    std::cout << "A notification daemon for DesQ, implementing XDG Notification Spec v1.2\n" << std::endl;
}


void createCLI( QCommandLineParser *parser ) {
    parser->addOption( {
                           { "?", "help" },
                           "Print this help text"
                       } );

    parser->addOption( {
                           { "v", "version" },
                           "Print application version and exit."
                       } );

    parser->addOption( {
                           { "u", "urgency" },
                           "Specifies the urgency level (low, normal, critical).",
                           "LEVEL"
                       } );

    parser->addOption( {
                           { "t", "expire-time" },
                           "Specifies the timeout in milliseconds at which to expire the notification.",
                           "TIME"
                       } );

    parser->addOption( {
                           { "r", "replace-id" },
                           "The notification with id that needs to be replaced with this one",
                           "ID"
                       } );

    parser->addOption( {
                           { "a", "app-name" },
                           "Specifies the app name for the notification",
                           "NAME"
                       } );

    parser->addOption( {
                           { "i", "icon" },
                           "Specifies an icon filename or stock icon to display.",
                           "ICON"
                       } );

    parser->addOption( {
                           { "c", "category" },
                           "Specifies the notification category.",
                           "TYPE"
                       } );

    parser->addOption( {
                           { "h", "hint" },
                           "Specifies basic extra data to pass. Valid types are int, double, string and byte.",
                           "TYPE:NAME:VALUE"
                       } );

    parser->addOption( {
                           { "A", "action" },
                           "Specifies the action the user can use to interact with the client",
                           "NAME:VALUE"
                       } );

    parser->addOption( {
                           { "x", "close" },
                           "Requests the notification server to close the notification ID.",
                           "id"
                       } );

    parser->addOption( {
                           { "p", "print-id" },
                           "Print the notification ID.",
                       } );

    parser->addOption( {
                           { "e", "transient" },
                           "Create a transient notification, ignoring the resident hint.",
                       } );

    parser->addOption( {
                           { "w", "wait" },
                           "Wait for the notification to be closed or for user interaction.",
                       } );

    parser->addOption( {
                           { "b", "capabilities" },
                           "Print ther server capabilities.",
                       } );

    parser->addOption( {
                           { "f", "server-info" },
                           "Print the server information.",
                       } );

    QCommandLineOption hiddenServerOpt(
        "server",
        "Start desq-notifier in server mode. If another notification server is running, this instance will close."
    );
    hiddenServerOpt.setFlags( QCommandLineOption::HiddenFromHelp );
    parser->addOption( hiddenServerOpt );

    parser->addPositionalArgument(
        "summary",
        "Summary of the notification",
        "<summary>"
    );

    parser->addPositionalArgument(
        "body",
        "Body of the notification",
        "[BODY]"
    );
}


QStringList getServerInformation() {
    /** Assuming that some-other notification daemon is running */
    QDBusInterface iface(
        "org.freedesktop.Notifications",
        "/org/freedesktop/Notifications",
        "org.freedesktop.Notifications",
        QDBusConnection::sessionBus()
    );

    if ( iface.isValid() ) {
        QVariantList args;
        QDBusMessage reply = iface.callWithArgumentList( QDBus::Block, "GetServerInformation", args );

        if ( ( reply.type() == QDBusMessage::ReplyMessage ) && ( reply.arguments().count() == 4 ) ) {
            return {
                reply.arguments().at( 0 ).toString(),
                reply.arguments().at( 1 ).toString(),
                reply.arguments().at( 2 ).toString(),
                reply.arguments().at( 3 ).toString()
            };
        }
    }

    return QStringList();
}


QStringList getServerCapabilities() {
    /** Assuming that some-other notification daemon is running */
    QDBusInterface iface(
        "org.freedesktop.Notifications",
        "/org/freedesktop/Notifications",
        "org.freedesktop.Notifications",
        QDBusConnection::sessionBus()
    );

    if ( iface.isValid() ) {
        QDBusReply<QStringList> reply = iface.call( QDBus::Block, "GetCapabilities" );

        if ( reply.isValid() ) {
            return reply.value();
        }
    }

    return QStringList();
}


bool isServiceRunning() {
    QDBusInterface session(
        "org.freedesktop.DBus",
        "/org/freedesktop/DBus",
        "org.freedesktop.DBus",
        QDBusConnection::sessionBus()
    );

    QDBusReply<QStringList> reply = session.call( "ListNames" );

    if ( reply.value().contains( "org.freedesktop.Notifications" ) ) {
        return true;
    }

    return false;
}
