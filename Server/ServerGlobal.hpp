/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtGui>
#include <QtWidgets>

#include <desqui/LayoutManager.hpp>
#include <DFSettings.hpp>
#include <wayqt/Registry.hpp>

extern DFL::Settings *notifySett;

extern WQt::Registry *wlRegistry;

/** Notification icon is a label. This is it's size */
extern int iconLabelSize;

/** The actual icon's size */
extern int iconImageSize;

/** All notificaitons will have a fixed width. */
extern int bubbleWidth;

/** Notifications expire after this time */
extern int defaultTimeOut;

/** UI Theme */
extern QString uiTheme;

/**
 * Theme paths.
 * Typically:
 *    - /usr/share/desq/themes/Notifier;
 *    - ~/.local/share/DesQ/Themes/Notifier
 */
extern QStringList themePaths;

namespace DesQ {
    namespace Notifier {
        class WidgetFactory;
        class ThemeManager;
    }
}

/** Layout manager, widget factory and the theme manager */
extern DesQUI::LayoutManager         *lytMgr;
extern DesQ::Notifier::WidgetFactory *wFactory;
extern DesQ::Notifier::ThemeManager  *themeMgr;
