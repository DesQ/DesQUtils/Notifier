/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <iostream>
#include <signal.h>

#include <DFXdg.hpp>
#include <DFUtils.hpp>
#include <DFNotifyWatcher.hpp>

#include <DFCoreApplication.hpp>
#include <DFApplication.hpp>

#include <desq/Utils.hpp>
#include <desq/desq-config.h>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>

#include "ClientGlobal.hpp"
#include "ServerGlobal.hpp"

#include "MainClient.hpp"

#include "UI.hpp"
#include "Misc.hpp"

DFL::Settings *notifySett;
int           iconLabelSize;
int           iconImageSize;
int           bubbleWidth;
int           defaultTimeOut;
QString       uiTheme;

WQt::Registry *wlRegistry = nullptr;

QStringList themePaths {
    DFL::XDG::xdgDataHome() + "DesQ/Themes/Notifier/",
    ThemePath "Notifier/",
};

DesQUI::LayoutManager         *lytMgr   = nullptr;
DesQ::Notifier::WidgetFactory *wFactory = nullptr;
DesQ::Notifier::ThemeManager  *themeMgr = nullptr;

int runServerCode( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Notifier.log" ).toLocal8Bit().data(), "a" );
    qInstallMessageHandler( DFL::Logger );

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Notifier started at" << QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );
    DFL::Application *app = new DFL::Application( argc, argv );

    app->setOrganizationName( "DesQ" );
    app->setApplicationName( "Notifier" );
    app->setApplicationVersion( PROJECT_VERSION );
    app->setDesktopFileName( "desq-notifier" );

    app->setQuitOnLastWindowClosed( false );

    app->interceptSignal( SIGSEGV, true );

    /** So that these can be used in QDBus, QHash, and QVariant, etc */
    qRegisterMetaType<DFL::NotifyWatcher::Notification>( "Notification" );
    qDBusRegisterMetaType<DFL::NotifyWatcher::Notification>();

    /** If another server is running somewhere else, we quit. */
    if ( isServiceRunning() ) {
        QStringList serverInfo = getServerInformation();

        if ( serverInfo.count() == 4 ) {
            if ( serverInfo.at( 1 ) == "DesQ" ) {
                qWarning() << "Another instance of this application is already running. Aborting...!";
            }

            else {
                qWarning() << serverInfo.at( 0 ).toStdString().c_str() << " from " << serverInfo.at( 1 ).toStdString().c_str();
                qWarning() << " is providing notification services. Aborting...!";
            }
        }

        else {
            qWarning() << "Another notification server is running is providing notification services. Aborting...!";
        }

        return 1;
    }

    if ( app->lockApplication() == false ) {
        qWarning() << "Failed to lock instance. You may not be able to connect to the running instance.";
    }

    app->setOrganizationName( "DesQ" );
    app->setApplicationName( "Notifier" );
    app->setApplicationVersion( PROJECT_VERSION );
    app->setDesktopFileName( "desq-notifier.desktop" );

    /** Waylnd Registry */
    wlRegistry = new WQt::Registry( WQt::Wayland::display() );
    wlRegistry->setup();

    /** Settings of our Notifier */
    notifySett = DesQ::Utils::initializeDesQSettings( "Notifier", "Notifier" );

    iconLabelSize  = ( int )notifySett->value( "IconLabelSize" );
    iconImageSize  = ( int )notifySett->value( "IconImageSize" );
    bubbleWidth    = ( int )notifySett->value( "BubbleWidth" );
    defaultTimeOut = ( int )notifySett->value( "DefaultTimeOut" );
    uiTheme        = ( QString )notifySett->value( "UI" );

    lytMgr   = new DesQUI::LayoutManager( QSize( bubbleWidth, 100 ) );
    wFactory = new DesQ::Notifier::WidgetFactory( QSize( bubbleWidth, 100 ) );
    themeMgr = new DesQ::Notifier::ThemeManager();

    lytMgr->setWidgetCreator( wFactory );
    themeMgr->setThemeName( uiTheme );

    /** Our notification watcher daemon */
    DesQ::Notifier::UI *OSD = new DesQ::Notifier::UI();

    /** Re-route the messages received by the app to the Notifier instance */
    QObject::connect( app, &DFL::Application::messageFromClient, OSD, &DesQ::Notifier::UI::handleMessages );

    return app->exec();
}
